#include "isr-util.h"
#include "packet-buffer.h"

#include <csp/arch/csp_time.h>
#include <csp/csp_buffer.h>
#include <csp/csp_error.h>

/* Number of packet buffer elements. */
#define PBUF_ELEMENTS 5

/* Statically allocated memory for the packet buffers. */
static v_csp_ground_link_pbuf pbufs[PBUF_ELEMENTS] = {};

/* The duration that a packet buffer is valid for. */
#define PBUF_TIMEOUT_MS 5000

v_csp_ground_link_pbuf* v_csp_ground_link_pbuf_new(uint32_t conn_id,
                                                   CSP_BASE_TYPE* task_woken) {
  uint32_t now = csp_get_ms_maybe_isr(task_woken);

  for (int i = 0; i < PBUF_ELEMENTS; i++) {
    v_csp_ground_link_pbuf* pbuf = pbufs + i;

    /* Free packet buffers that timed out. */
    if (pbuf->packet != NULL) {
      if (now - pbuf->last_used > PBUF_TIMEOUT_MS) {
        v_csp_ground_link_pbuf_free(pbuf, task_woken);
      }
    }

    /* Use the first packet buffer that is not already in use. */
    if (!pbuf->is_in_use) {
      pbuf->is_in_use = true;
      pbuf->conn_id = conn_id;
      pbuf->last_used = now;
      return pbuf;
    }
  }

  return NULL;
}

v_csp_ground_link_pbuf* v_csp_ground_link_pbuf_find(uint32_t conn_id,
                                                    CSP_BASE_TYPE* task_woken) {
  for (int i = 0; i < PBUF_ELEMENTS; i++) {
    v_csp_ground_link_pbuf* pbuf = pbufs + i;
    if (pbuf->is_in_use && pbuf->conn_id == conn_id) {
      pbuf->last_used = csp_get_ms_maybe_isr(task_woken);
      return pbuf;
    }
  }

  return NULL;
}

void v_csp_ground_link_pbuf_free(v_csp_ground_link_pbuf* buf,
                                 CSP_BASE_TYPE* task_woken) {
  if (buf->packet != NULL) {
    csp_buffer_free_maybe_isr(buf->packet, task_woken);
    buf->packet = NULL;
  }

  buf->is_in_use = false;
}

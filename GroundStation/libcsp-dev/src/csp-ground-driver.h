
#ifndef _CSP_GROUND_DRIVER_H_
#define _CSP_GROUND_DRIVER_H_

#include <csp/csp.h>
#include <csp/interfaces/csp_if_can.h>

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

#include "csp-ground-link-iface.h"

#define MAX_BYTES_IN_FRAME 32

#define ENCODE_HEX_CHAR(b) (((b) <= 9) ? ((b) + '0') : ((b) -0xa + 'a'))
#define IS_HEX_CHAR(c)                                                         \
  (((c) >= '0' && (c) <= '9') || ((c) >= 'a' && (c) <= 'f'))
#define DECODE_HEX_CHAR(c) (((c) <= '9') ? ((c) - '0') : (0xa + (c) - 'a'))

 
 typedef struct {
    char name[CSP_IFLIST_NAME_MAX + 1];
    csp_iface_t iface;
    csp_ground_interface_data_t ifdata;
    pthread_t rx_thread;
    int socket;
} ground_context_t;


// static void csp_virtual_link_free(ground_context_t* ctx);
// static int csp_ground_tx_frame(void* driver_data, const uint8_t* data,
//                                      uint16_t dlc);
// static int csp_virtual_link_handshake(ground_context_t* ctx);

int csp_ground_init_and_add_interface(const char* ifname, csp_iface_t** iface); 



#endif

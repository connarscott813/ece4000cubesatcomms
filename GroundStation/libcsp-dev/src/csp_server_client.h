#ifndef _CSP_SERVER_CLIENT_H_
#define _CSP_SERVER_CLIENT_H_

#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include <csp/csp.h>
#include <csp/arch/csp_thread.h>
#include <csp/interfaces/csp_if_zmqhub.h>
#include <csp/csp_debug.h>

#include "csp-ground-driver.h"

/* Server port, the port the server listens on for incoming connections from the client. */
#define CSP_SERVER_PORT  1
#define CSP_SERVER_ADDR 255



void server(void);
void client(void);
uint32_t returnNumReceived(void);


#endif

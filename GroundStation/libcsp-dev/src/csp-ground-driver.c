#include "csp-ground-driver.h"

static void csp_virtual_link_free(ground_context_t* ctx) {
    if (ctx->socket != -1) {
        close(ctx->socket);
    }
    free(ctx);
}

static void* csp_ground_rx_thread(void* arg) {
    ground_context_t* ctx = arg;
    char recv_buf[2 * MAX_BYTES_IN_FRAME];
    size_t recv_buf_len = 0;

    while (1) {
        /* Technically, the buffer could also be filled entirely, in which case
        this assertion will fail. However, that means that a protocol error
        occurred during simulation, and that should never be the case. */
        assert(recv_buf_len < sizeof(recv_buf));
        size_t n_remaining = sizeof(recv_buf) - recv_buf_len;

        ssize_t n_recv = recv(ctx->socket, recv_buf + recv_buf_len, n_remaining, 0);
        if (n_recv < 0) {
        printf("%s[%s]: read() failed, errno %d: %s", __FUNCTION__,
                        ctx->name, errno, strerror(errno));
        // TODO: Should we really continue here?
        exit(0);
        }

        recv_buf_len += n_recv;

        char* end_of_line = memchr(recv_buf, '\n', recv_buf_len);
        if (end_of_line == NULL) {
        continue;
        }

        size_t line_len = end_of_line - recv_buf + 1;
        memmove(recv_buf, recv_buf + line_len, recv_buf_len - line_len);
        recv_buf_len -= line_len;
    }

    pthread_exit(NULL);
}

static int csp_ground_tx_frame(void* driver_data, const uint8_t* data,
                                     uint16_t dlc) {
    ground_context_t* ctx = driver_data;

    if (dlc > MAX_BYTES_IN_FRAME) {
        return CSP_ERR_INVAL;
    }

    char data_hex[2 * MAX_BYTES_IN_FRAME + 1] = { 0 };
    for (uint_fast16_t i = 0; i < dlc; i++) {
        data_hex[2 * i] = ENCODE_HEX_CHAR((data[i] >> 4) & 0xf);
        data_hex[2 * i + 1] = ENCODE_HEX_CHAR(data[i] & 0xf);
    }

    char line[2 * MAX_BYTES_IN_FRAME + 1];
    int n = snprintf(line, sizeof(line),"%s\n", data_hex);
    assert(n > 0 && n < (int) sizeof(line));
    size_t total_size = (size_t) n;

    /* Now send the line. */
    // TODO: Figure out if this can block the main thread.
    uint_fast16_t elapsed_ms = 0;
    size_t remainder_offset = 0;
    while (remainder_offset < total_size) {
        ssize_t n_sent = send(ctx->socket, line + remainder_offset,
                            total_size - remainder_offset, 0);
        if (n_sent < 0) {
        if ((errno != ENOBUFS) || (elapsed_ms >= 1000)) {
            printf("%s[%s]: send() failed, errno %d: %s", __FUNCTION__,
                        ctx->name, errno, strerror(errno));
            return CSP_ERR_TX;
        }
        usleep(5 * 1000);
        elapsed_ms += 5;
        } else {
        remainder_offset += n_sent;
        }
    }

  return CSP_ERR_NONE;
}

static int csp_virtual_link_handshake(ground_context_t* ctx) {
  if ((ctx->socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    return CSP_ERR_DRIVER;
  }

  struct sockaddr_in socket_addr;
  socket_addr.sin_family = AF_INET;
  socket_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  socket_addr.sin_port = htons(5554);

  if (connect(ctx->socket, (struct sockaddr*) &socket_addr,
              sizeof(socket_addr)) < 0) {
    return CSP_ERR_DRIVER;
  }

  /* For the initial handshake, we need to tell the server who we are and what
     link we are trying to connect to. */
  char msgbuf[64];
  int hello_msg_len =
      snprintf(msgbuf, sizeof(msgbuf), "connect;addr=%u;link=%s\n",
         csp_get_address(), ctx->name);
  assert(hello_msg_len > 0 && hello_msg_len < (int) sizeof(msgbuf));
  ssize_t n_sent = send(ctx->socket, msgbuf, hello_msg_len, 0);
  if (n_sent < 0) {
    ctx->socket = -1;
    return CSP_ERR_DRIVER;
  }

  /* This is not technically guaranteed to be true, but good enough for
     simulations on the ground. */
  assert(n_sent == hello_msg_len);

  /* The server will either respond with exactly three bytes ("ok\n"), or close
     the connection. */
  ssize_t n_recv = recv(ctx->socket, msgbuf, 3, 0);
  /* TODO: Allow partial transmissions? */
  if (n_recv != 3) {
    return CSP_ERR_DRIVER;
  }

  if (msgbuf[0] != 'o' || msgbuf[1] != 'k' || msgbuf[2] != '\n') {
    return CSP_ERR_DRIVER;
  }

  return CSP_ERR_NONE;
}

int csp_ground_init_and_add_interface(const char* ifname,
                                                   csp_iface_t** iface) {
    ground_context_t* ctx = calloc(1, sizeof(ground_context_t));
    if (ctx == NULL) {
        return CSP_ERR_NOMEM;
    }

    strncpy(ctx->name, ifname, sizeof(ctx->name) - 1);
    ctx->iface.name = ctx->name;
    ctx->iface.interface_data = &ctx->ifdata;
    ctx->iface.driver_data = ctx;
    ctx->ifdata.tx_func = csp_ground_tx_frame;
    ctx->ifdata.max_frame_size = MAX_BYTES_IN_FRAME;
    
    
    int error;


    /* Add interface to CSP. */
    if ((error = v_csp_ground_link_add_interface(&ctx->iface)) != CSP_ERR_NONE) {
        printf("%s[%s]: v_csp_ground_link_add_interface() failed, error: %d\r\n",
                    __FUNCTION__, ctx->name, error);
        csp_virtual_link_free(ctx);
        return error;
    }

    /* Start receiving frames. */
    if (pthread_create(&ctx->rx_thread, NULL, csp_ground_rx_thread, ctx)) {
        printf("%s[%s]: pthread_create() failed, error: %s\r\n", __FUNCTION__,
                    ctx->name, strerror(errno));
        /* We cannot free the interface because we cannot remove it from CSP.
        Yes, this is bad. */
        return CSP_ERR_NOMEM;
    }

    if (iface != NULL) {
        *iface = &ctx->iface;
    }

  return CSP_ERR_NONE;
}

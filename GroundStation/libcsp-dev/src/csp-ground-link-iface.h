#ifndef __CSP_GROUND_LINK_IFACE__H__
#define __CSP_GROUND_LINK_IFACE__H__

#include <csp/csp_interface.h>

/* We define a protocol that is similiar to the CAN Fragmentation Protocol (CFP)
   that is used by the libcsp CAN interface. Each frame begins with a 32 bit
   header:

   +---------+-------------+-------+-----------+------------+
   | 0 ... 4 |   5 ... 9   |  10   | 11 ... 18 | 19 ... 31  |
   +---------+-------------+-------+-----------+------------+
   | Source  | Destination | Flags | Remaining | Identifier |
   +---------+-------------+-------+-----------+------------+

   This is exactly the same structure that CFP uses, except that identifiers
   consist of 12 bits instead of 10 only. However, ground link frames are much
   longer than the maximum of 64 bits for CAN frames, therefore, the MTU should
   be significantly larger here. */

// clang-format off

#define CSP_GROUND_IFACE_DEFAULT_NAME "Edward"

#define V_GLFP_HOST_BITS    5
#define V_GLFP_FLAGS_BITS   1
#define V_GLFP_REMAIN_BITS  8
#define V_GLFP_IDENT_BITS  12

#define V_GLFP_SOURCE_OFFSET 0
#define V_GLFP_DEST_OFFSET   (V_GLFP_SOURCE_OFFSET + V_GLFP_HOST_BITS)
#define V_GLFP_FLAGS_OFFSET  (V_GLFP_DEST_OFFSET   + V_GLFP_HOST_BITS)
#define V_GLFP_REMAIN_OFFSET (V_GLFP_FLAGS_OFFSET  + V_GLFP_FLAGS_BITS)
#define V_GLFP_IDENT_OFFSET  (V_GLFP_REMAIN_OFFSET + V_GLFP_REMAIN_BITS)

#define V_GLFP_MAKE_SOURCE(s) ((s) << V_GLFP_SOURCE_OFFSET)
#define V_GLFP_MAKE_DEST(d)   ((d) << V_GLFP_DEST_OFFSET)
#define V_GLFP_MAKE_FLAGS(f)  ((f) << V_GLFP_FLAGS_OFFSET)
#define V_GLFP_MAKE_REMAIN(r) ((r) << V_GLFP_REMAIN_OFFSET)
#define V_GLFP_MAKE_IDENT(i)  ((i) << V_GLFP_IDENT_OFFSET)

#define V_GLFP_SOURCE_MASK  V_GLFP_MAKE_SOURCE ( (1 << V_GLFP_HOST_BITS)   - 1)
#define V_GLFP_DEST_MASK    V_GLFP_MAKE_DEST   ( (1 << V_GLFP_HOST_BITS)   - 1)
#define V_GLFP_FLAGS_MASK   V_GLFP_MAKE_FLAGS  ( (1 << V_GLFP_FLAGS_BITS)  - 1)
#define V_GLFP_REMAIN_MASK  V_GLFP_MAKE_REMAIN ( (1 << V_GLFP_REMAIN_BITS) - 1)
#define V_GLFP_IDENT_MASK   V_GLFP_MAKE_IDENT  ( (1 << V_GLFP_IDENT_BITS)  - 1)

#define V_GLFP_ID_CONN_MASK \
    (V_GLFP_SOURCE_MASK | V_GLFP_DEST_MASK | V_GLFP_IDENT_MASK)

#define V_GLFP_SOURCE(id) ((V_GLFP_SOURCE_MASK & (id)) >> V_GLFP_SOURCE_OFFSET)
#define V_GLFP_DEST(id)   ((V_GLFP_DEST_MASK   & (id)) >> V_GLFP_DEST_OFFSET)
#define V_GLFP_FLAGS(id)  ((V_GLFP_FLAGS_MASK  & (id)) >> V_GLFP_FLAGS_OFFSET)
#define V_GLFP_REMAIN(id) ((V_GLFP_REMAIN_MASK & (id)) >> V_GLFP_REMAIN_OFFSET)
#define V_GLFP_IDENT(id)  ((V_GLFP_IDENT_MASK  & (id)) >> V_GLFP_IDENT_OFFSET)

#define V_GLFP_FLAG_MORE 1

// clang-format on

typedef uint32_t v_csp_glfp_header;

typedef int (*csp_ground_driver_tx_t)(void* driver_data,
                                       const uint8_t* data,
                                       uint16_t length);

 typedef struct {
    uint_fast16_t next_packet_id;
    csp_ground_driver_tx_t tx_func;
    uint16_t max_frame_size;
}csp_ground_interface_data_t;

int v_csp_ground_link_add_interface(csp_iface_t* iface);

int v_csp_ground_link_tx(const csp_route_t* ifroute, csp_packet_t* packet);

int v_csp_ground_link_rx(csp_iface_t* iface, const uint8_t* data,
                         uint16_t length, CSP_BASE_TYPE* pxTaskWoken);

#endif /* __CSP_GROUND_LINK_IFACE__H__ */

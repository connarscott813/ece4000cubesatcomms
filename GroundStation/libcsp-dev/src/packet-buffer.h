#include <csp/csp_platform.h>

typedef struct {
  bool is_in_use;
  uint32_t conn_id;
  uint32_t last_used;

  csp_packet_t* packet;
  uint16_t received_bytes;
  uint_fast8_t remaining_frames;
} v_csp_ground_link_pbuf;

v_csp_ground_link_pbuf* v_csp_ground_link_pbuf_new(uint32_t conn_id,
                                                   CSP_BASE_TYPE* task_woken);

v_csp_ground_link_pbuf* v_csp_ground_link_pbuf_find(uint32_t conn_id,
                                                    CSP_BASE_TYPE* task_woken);

void v_csp_ground_link_pbuf_free(v_csp_ground_link_pbuf* buf,
                                 CSP_BASE_TYPE* task_woken);

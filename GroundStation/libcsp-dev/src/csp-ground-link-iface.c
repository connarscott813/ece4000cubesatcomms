#include "isr-util.h"
#include "packet-buffer.h"

#include "csp-ground-link-iface.h"

#include <csp/arch/csp_semaphore.h>
#include <csp/csp.h>
#include <csp/csp_endian.h>

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define MAX_GROUND_LINK_FRAME_SIZE 256

/* Similar to the CAN Fragmentation Protocol, packets in the GLFP also begin
   with the CSP header (csp_id_t) and the packet length (uint16_t). */
#define GLFP_CSP_OVERHEAD (sizeof(csp_id_t) + sizeof(uint16_t))

#define GLFP_HEADER_SIZE sizeof(v_csp_glfp_header)

/* The number of frames is limited by the size of the 'remaining' field. */
#define GLFP_MAX_FRAMES (1 << V_GLFP_REMAIN_BITS)

int v_csp_ground_link_rx(csp_iface_t* iface, const uint8_t* data,
                         uint16_t length, CSP_BASE_TYPE* pxTaskWoken) {
  /* Copy the header and fix the byte order if necessary. */
  v_csp_glfp_header header;
  if (length < sizeof(header)) {
    csp_log_warn("%s[%s]: received frame is too short", __FUNCTION__,
                 iface->name);
    return CSP_ERR_NONE;
  }
  memcpy(&header, data, sizeof(header));
  header = csp_ntoh32(header);

  /* Skip the header for the rest of the function. */
  data += sizeof(header);
  length -= sizeof(header);

  /* The connection is uniquely identified by sender and receiver address and
     the 'ident' field. */
  uint32_t conn_id = header & V_GLFP_ID_CONN_MASK;

  /* Try to match the frame to an existing packet buffer. */
  v_csp_ground_link_pbuf* pbuf =
      v_csp_ground_link_pbuf_find(conn_id, pxTaskWoken);
  if (pbuf == NULL) {
    if ((V_GLFP_FLAGS(header) & V_GLFP_FLAG_MORE) == 0) {
      /* This appears to be the beginning of a new packet, so try to allocate
         a new packet buffer. */
      pbuf = v_csp_ground_link_pbuf_new(conn_id, pxTaskWoken);
      if (pbuf == NULL) {
        iface->rx_error++;
        csp_log_warn("%s[%s]: cannot allocate a new packet buffer",
                     __FUNCTION__, iface->name);
        return CSP_ERR_NOMEM;
      }
    } else {
      /* This is not the beginning of a new packet, but we also don't have
         previous fragments. */
      iface->frame++;
      csp_log_warn("%s[%s]: missing previous fragments", __FUNCTION__,
                   iface->name);
      return CSP_ERR_NONE;
    }
  }

  /* Check if this frame is the beginning of a new packet. */
  if ((V_GLFP_FLAGS(header) & V_GLFP_FLAG_MORE) == 0) {
    /* The first frame must contain the CSP header in addition to the GLFP
       header. */
    if (length < GLFP_CSP_OVERHEAD) {
      iface->frame++;
      v_csp_ground_link_pbuf_free(pbuf, pxTaskWoken);
      return CSP_ERR_NONE;
    }

    /* If we found an existing packet buffer, it might still have a packet
       attached. In that case, we can reuse it. */
    if (pbuf->packet != NULL) {
      /* Reuse the buffer. */
      iface->frame++;
    } else {
      /* If no buffer was attached, allocate a new one. */
      pbuf->packet = csp_buffer_get_maybe_isr(0, pxTaskWoken);
      if (pbuf->packet == NULL) {
        csp_log_warn("%s[%s]: failed to allocate csp buffer", __FUNCTION__,
                     iface->name);
        iface->frame++;
        v_csp_ground_link_pbuf_free(pbuf, pxTaskWoken);
        return CSP_ERR_NONE;
      }
    }

    /* Copy CSP identifier (header). */
    memcpy(&(pbuf->packet->id), data, sizeof(pbuf->packet->id));
    pbuf->packet->id.ext = csp_ntoh32(pbuf->packet->id.ext);

    /* Copy CSP length (of data). */
    memcpy(&(pbuf->packet->length), data + sizeof(csp_id_t),
           sizeof(pbuf->packet->length));
    pbuf->packet->length = csp_ntoh16(pbuf->packet->length);

    /* Check that the packet fits into the MTU and into the allocated buffer. */
    if (pbuf->packet->length > iface->mtu ||
        pbuf->packet->length > csp_buffer_data_size()) {
      iface->rx_error++;
      csp_log_warn("%s[%s]: received packet is too long (%u > %u or %u > %zu)",
                   __FUNCTION__, iface->name, pbuf->packet->length, iface->mtu,
                   pbuf->packet->length, csp_buffer_data_size());
      v_csp_ground_link_pbuf_free(pbuf, pxTaskWoken);
      return CSP_ERR_NONE;
    }

    /* Shift the data buffer so the rest of this function ignores the CSP
       overhead. */
    data += GLFP_CSP_OVERHEAD;
    length -= GLFP_CSP_OVERHEAD;

    /* Reset RX count. */
    pbuf->received_bytes = 0;

    /* Set 'remain' field - increment to include begin packet */
    pbuf->remaining_frames = V_GLFP_REMAIN(header) + 1;
  }

  /* Check 'remain' field match. */
  // TODO: Why is the cast necessary here?
  if (V_GLFP_REMAIN(header) != (uint32_t) pbuf->remaining_frames - 1) {
    // csp_log_error("CAN frame lost in CSP packet");
    v_csp_ground_link_pbuf_free(pbuf, pxTaskWoken);
    iface->frame++;
    csp_log_warn("%s[%s]: remain incorrect", __FUNCTION__, iface->name);
    return CSP_ERR_NONE;
  }

  /* Decrement number of remaining frames. */
  pbuf->remaining_frames--;

  // TODO: Don't we need to check whether remaining_frames == 0?

  /* Check for overflow */
  if (pbuf->received_bytes + length > pbuf->packet->length) {
    // csp_log_error("RX buffer overflow");
    iface->frame++;
    v_csp_ground_link_pbuf_free(pbuf, pxTaskWoken);
    csp_log_warn("%s[%s]: frame would overflow packet", __FUNCTION__,
                 iface->name);
    return CSP_ERR_NONE;
  }

  /* Copy all available data bytes into buffer. */
  memcpy(pbuf->packet->data + pbuf->received_bytes, data, length);
  pbuf->received_bytes += length;

  /* Check if the packet is complete. */
  if (pbuf->received_bytes == pbuf->packet->length) {
    /* Data is available */
    csp_qfifo_write(pbuf->packet, iface, pxTaskWoken);

    /* Drop packet buffer reference */
    pbuf->packet = NULL;

    /* Free packet buffer */
    v_csp_ground_link_pbuf_free(pbuf, pxTaskWoken);
  }

  return CSP_ERR_NONE;
}

static inline void copy_fragmentation_header(uint8_t* bytes,
                                             v_csp_glfp_header header) {
  header = csp_hton32(header);
  memcpy(bytes, &header, GLFP_HEADER_SIZE);
}

int v_csp_ground_link_tx(const csp_route_t* ifroute, csp_packet_t* packet) {
  csp_iface_t* iface = ifroute->iface;
  csp_ground_interface_data_t* ifdata = iface->interface_data;

  /* Get an unique GLFP id - this should be locked to prevent access from
   * multiple tasks */
  // TODO: Use __atomic_fetch_add here
  const uint_fast16_t ident =
      (ifdata->next_packet_id++) & ((1 << V_GLFP_IDENT_BITS) - 1);

  /* Ensure this does not violate the MTU. */
  if (packet->length > iface->mtu) {
    return CSP_ERR_TX;
  }

  /* Insert destination node/via address into the CFP destination field */
  const uint8_t dest =
      (ifroute->via != CSP_NO_VIA_ADDRESS) ? ifroute->via : packet->id.dst;

  uint16_t bytes_per_frame = ifdata->max_frame_size - GLFP_HEADER_SIZE;
  uint8_t remaining =
      (packet->length + GLFP_CSP_OVERHEAD - 1) / bytes_per_frame;

  /* Start the frame with the fragmentation header. */
  uint8_t frame_buf[MAX_GROUND_LINK_FRAME_SIZE];
  copy_fragmentation_header(
      frame_buf, V_GLFP_MAKE_SOURCE(packet->id.src) | V_GLFP_MAKE_DEST(dest) |
                     V_GLFP_MAKE_FLAGS(0) | V_GLFP_MAKE_REMAIN(remaining) |
                     V_GLFP_MAKE_IDENT(ident));

  /* Copy CSP headers. */
  const uint32_t csp_id_be = csp_hton32(packet->id.ext);
  const uint16_t csp_length_be = csp_hton16(packet->length);
  memcpy(frame_buf + GLFP_HEADER_SIZE, &csp_id_be, sizeof(csp_id_be));
  memcpy(frame_buf + GLFP_HEADER_SIZE + sizeof(csp_id_be), &csp_length_be,
         sizeof(csp_length_be));

  /* Copy the actual packet data (or as much as we can fit into the first
     frame). */
  const uint16_t first_frame_data_start = GLFP_HEADER_SIZE + GLFP_CSP_OVERHEAD;
  const uint16_t avail = ifdata->max_frame_size - first_frame_data_start;
  uint16_t n_bytes = (packet->length <= avail) ? packet->length : avail;
  memcpy(frame_buf + first_frame_data_start, packet->data, n_bytes);

  /* Increment tx counter */
  uint16_t tx_count =
      n_bytes;  // TODO: Why isn't this iface->tx? Is that updated elsewhere?

  /* Send first frame */
  if (ifdata->tx_func(iface->driver_data, frame_buf,
                      first_frame_data_start + n_bytes) != CSP_ERR_NONE) {
    iface->tx_error++;
    return CSP_ERR_DRIVER;
  }

  /* Send next frames if not complete */
  while (tx_count < packet->length) {
    /* Calculate frame data bytes */
    n_bytes = (packet->length - tx_count >= bytes_per_frame)
                  ? bytes_per_frame
                  : packet->length - tx_count;
    remaining = (packet->length - tx_count - n_bytes + bytes_per_frame - 1) /
                bytes_per_frame;

    copy_fragmentation_header(
        frame_buf, V_GLFP_MAKE_SOURCE(packet->id.src) | V_GLFP_MAKE_DEST(dest) |
                       V_GLFP_MAKE_FLAGS(V_GLFP_FLAG_MORE) |
                       V_GLFP_MAKE_REMAIN(remaining) |
                       V_GLFP_MAKE_IDENT(ident));
    memcpy(frame_buf + GLFP_HEADER_SIZE, packet->data + tx_count, n_bytes);

    /* Increment tx counter */
    tx_count += n_bytes;

    /* Send frame */
    if (ifdata->tx_func(iface->driver_data, frame_buf,
                        GLFP_HEADER_SIZE + n_bytes) != CSP_ERR_NONE) {
      // csp_log_warn("Failed to send CAN frame in Tx callback");
      iface->tx_error++;
      return CSP_ERR_DRIVER;
    }
  }

  csp_buffer_free(packet);
  return CSP_ERR_NONE;
}

int v_csp_ground_link_add_interface(csp_iface_t* iface) {
  assert(iface != NULL);
  assert(iface->name != NULL);

  csp_ground_interface_data_t* ifdata = iface->interface_data;
  assert(ifdata != NULL);
  assert(ifdata->tx_func != NULL);

  if (ifdata->max_frame_size > MAX_GROUND_LINK_FRAME_SIZE) {
    ifdata->max_frame_size = MAX_GROUND_LINK_FRAME_SIZE;
  }

  uint16_t bytes_per_frame = ifdata->max_frame_size - sizeof(v_csp_glfp_header);
  uint16_t max_mtu = (GLFP_MAX_FRAMES * bytes_per_frame) - GLFP_CSP_OVERHEAD;
  if ((iface->mtu == 0) || (iface->mtu > max_mtu)) {
    iface->mtu = max_mtu;
  }

  ifdata->next_packet_id = 0;  // TODO: Initialize this randomly.
  iface->nexthop = v_csp_ground_link_tx;

  return csp_iflist_add(iface);
}

#include <csp/csp_buffer.h>
#include <csp/arch/csp_time.h>

static inline uint32_t csp_get_ms_maybe_isr(CSP_BASE_TYPE* task_woken) {
  if (task_woken != NULL) {
    return csp_get_ms_isr();
  } else {
    return csp_get_ms();
  }
}

static inline void* csp_buffer_get_maybe_isr(uint32_t data_size,
                                             CSP_BASE_TYPE* task_woken) {
  if (task_woken != NULL) {
    return csp_buffer_get_isr(data_size);
  } else {
    return csp_buffer_get(data_size);
  }
}

static inline void csp_buffer_free_maybe_isr(csp_packet_t* packet,
                                             CSP_BASE_TYPE* task_woken) {
  if (task_woken != NULL) {
    csp_buffer_free_isr(packet);
  } else {
    csp_buffer_free(packet);
  }
}

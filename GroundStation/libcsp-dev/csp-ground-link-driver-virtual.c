#include <violet-drivers/csp-ground-link-driver-virtual.h>

#include <violet-api/csp-ground-link-iface.h>
#include <violet-api/lang-util.h>
#include <violet-api/subsystems.h>

#include <csp/arch/csp_thread.h>
#include <csp/csp.h>
#include <csp/interfaces/csp_if_can.h>

// TODO: Reduce as much as possible
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define VIRTUAL_GROUND_LINK_SERVER_ADDR 0x7f000001
#define VIRTUAL_GROUND_LINK_SERVER_PORT 49193

typedef struct {
  char name[CSP_IFLIST_NAME_MAX + 1];
  csp_iface_t iface;
  v_csp_ground_link_interface_data_t ifdata;
  pthread_t rx_thread;
  int socket;
} link_context_t;

#define LINK_FRAME_LINE_START "frame;data="
#define MAX_BYTES_IN_LINK_FRAME 32

#define ENCODE_HEX_CHAR(b) (((b) <= 9) ? ((b) + '0') : ((b) -0xa + 'a'))
#define IS_HEX_CHAR(c)                                                         \
  (((c) >= '0' && (c) <= '9') || ((c) >= 'a' && (c) <= 'f'))
#define DECODE_HEX_CHAR(c) (((c) <= '9') ? ((c) - '0') : (0xa + (c) - 'a'))

static void csp_virtual_link_free(link_context_t* ctx) {
  if (ctx->socket != -1) {
    close(ctx->socket);
  }
  free(ctx);
}

static void csp_virtual_link_decode(link_context_t* ctx, const char* line,
                                    size_t len) {
  /* We can already check that the length is valid here. */
  uint16_t min_len = strlen(LINK_FRAME_LINE_START);
  uint16_t max_len = min_len + 2 * MAX_BYTES_IN_LINK_FRAME;
  if (len < min_len || len > max_len) {
    return;
  }

  /* The line must begin with "frame;data=". */
  if (memcmp(line, LINK_FRAME_LINE_START, strlen(LINK_FRAME_LINE_START)) != 0) {
    return;
  }
  line += strlen(LINK_FRAME_LINE_START);

  /* The remainder is the hex representation of the data, and must, therefore,
     consist of an even number of characters. */
  uint16_t dlc2 = len - min_len;
  if (dlc2 % 2 != 0) {
    return;
  }

  uint16_t dlc = dlc2 / 2;
  uint8_t data[MAX_BYTES_IN_LINK_FRAME];
  for (uint_fast16_t i = 0; i < dlc; i++) {
    if (!IS_HEX_CHAR(line[2 * i]) || !IS_HEX_CHAR(line[2 * i + 1])) {
      return;
    }
    data[i] =
        (DECODE_HEX_CHAR(line[2 * i]) << 4) | DECODE_HEX_CHAR(line[2 * i + 1]);
  }

  /* Pass the received frame to the CAN interface implementation. */
  int error = v_csp_ground_link_rx(&ctx->iface, data, dlc, NULL);
  if (error == CSP_ERR_NONE) {
    csp_log_info("v_csp_ground_link_rx succeeded\n");
  } else {
    csp_log_error("v_csp_ground_link_rx failed: %d\n", error);
  }
}

static void* csp_virtual_link_rx_thread(void* arg) {
  link_context_t* ctx = arg;
  char recv_buf[sizeof(LINK_FRAME_LINE_START) + 2 * MAX_BYTES_IN_LINK_FRAME];
  size_t recv_buf_len = 0;

  while (1) {
    /* Technically, the buffer could also be filled entirely, in which case
       this assertion will fail. However, that means that a protocol error
       occurred during simulation, and that should never be the case. */
    assert(recv_buf_len < sizeof(recv_buf));
    size_t n_remaining = sizeof(recv_buf) - recv_buf_len;

    ssize_t n_recv = recv(ctx->socket, recv_buf + recv_buf_len, n_remaining, 0);
    if (n_recv < 0) {
      csp_log_error("%s[%s]: read() failed, errno %d: %s", __FUNCTION__,
                    ctx->name, errno, strerror(errno));
      // TODO: Should we really continue here?
      continue;
    }

    recv_buf_len += n_recv;

    char* end_of_line = memchr(recv_buf, '\n', recv_buf_len);
    if (end_of_line == NULL) {
      continue;
    }

    size_t line_len = end_of_line - recv_buf + 1;
    csp_virtual_link_decode(ctx, recv_buf, line_len - 1);
    memmove(recv_buf, recv_buf + line_len, recv_buf_len - line_len);
    recv_buf_len -= line_len;
  }

  UNREACHABLE();
}

static int csp_virtual_link_tx_frame(void* driver_data, const uint8_t* data,
                                     uint16_t dlc) {
  link_context_t* ctx = driver_data;

  if (dlc > MAX_BYTES_IN_LINK_FRAME) {
    return CSP_ERR_INVAL;
  }

  char data_hex[2 * MAX_BYTES_IN_LINK_FRAME + 1] = { 0 };
  for (uint_fast16_t i = 0; i < dlc; i++) {
    data_hex[2 * i] = ENCODE_HEX_CHAR((data[i] >> 4) & 0xf);
    data_hex[2 * i + 1] = ENCODE_HEX_CHAR(data[i] & 0xf);
  }

  char line[sizeof(LINK_FRAME_LINE_START) + 2 * MAX_BYTES_IN_LINK_FRAME + 1];
  int n = snprintf(line, sizeof(line), LINK_FRAME_LINE_START "%s\n", data_hex);
  assert(n > 0 && n < (int) sizeof(line));
  size_t total_size = (size_t) n;

  /* Now send the line. */
  // TODO: Figure out if this can block the main thread.
  uint_fast16_t elapsed_ms = 0;
  size_t remainder_offset = 0;
  while (remainder_offset < total_size) {
    ssize_t n_sent = send(ctx->socket, line + remainder_offset,
                          total_size - remainder_offset, 0);
    if (n_sent < 0) {
      if ((errno != ENOBUFS) || (elapsed_ms >= 1000)) {
        csp_log_warn("%s[%s]: send() failed, errno %d: %s", __FUNCTION__,
                     ctx->name, errno, strerror(errno));
        return CSP_ERR_TX;
      }
      csp_sleep_ms(5);
      elapsed_ms += 5;
    } else {
      remainder_offset += n_sent;
    }
  }

  return CSP_ERR_NONE;
}

static int csp_virtual_link_handshake(link_context_t* ctx) {
  if ((ctx->socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    return CSP_ERR_DRIVER;
  }

  struct sockaddr_in remote_addr;
  remote_addr.sin_family = AF_INET;
  remote_addr.sin_addr.s_addr = htonl(VIRTUAL_GROUND_LINK_SERVER_ADDR);
  remote_addr.sin_port = htons(VIRTUAL_GROUND_LINK_SERVER_PORT);

  if (connect(ctx->socket, (struct sockaddr*) &remote_addr,
              sizeof(remote_addr)) < 0) {
    return CSP_ERR_DRIVER;
  }

  /* Find out if we are on the ground or in orbit. */
  const char* own_loc;
  switch (v_subsystem_get_type(v_subsystem_id_self())) {
  case V_SUBSYSTEM_TYPE_SATELLITE:
    own_loc = "satellite";
    break;
  case V_SUBSYSTEM_TYPE_GROUND_STATION:
    own_loc = "ground";
    break;
  default:
    UNREACHABLE();
  }

  /* For the initial handshake, we need to tell the server who we are and what
     link we are trying to connect to. */
  char msgbuf[64];
  int hello_msg_len =
      snprintf(msgbuf, sizeof(msgbuf), "connect;loc=%s;addr=%u;link=%s\n",
               own_loc, csp_get_address(), ctx->name);
  assert(hello_msg_len > 0 && hello_msg_len < (int) sizeof(msgbuf));
  ssize_t n_sent = send(ctx->socket, msgbuf, hello_msg_len, 0);
  if (n_sent < 0) {
    ctx->socket = -1;
    return CSP_ERR_DRIVER;
  }

  /* This is not technically guaranteed to be true, but good enough for
     simulations on the ground. */
  assert(n_sent == hello_msg_len);

  /* The server will either respond with exactly three bytes ("ok\n"), or close
     the connection. */
  ssize_t n_recv = recv(ctx->socket, msgbuf, 3, 0);
  /* TODO: Allow partial transmissions? */
  if (n_recv != 3) {
    return CSP_ERR_DRIVER;
  }

  if (msgbuf[0] != 'o' || msgbuf[1] != 'k' || msgbuf[2] != '\n') {
    return CSP_ERR_DRIVER;
  }

  return CSP_ERR_NONE;
}

int csp_virtual_ground_link_init_and_add_interface(const char* ifname,
                                                   csp_iface_t** iface) {
  link_context_t* ctx = calloc(1, sizeof(link_context_t));
  if (ctx == NULL) {
    return CSP_ERR_NOMEM;
  }

  strncpy(ctx->name, ifname, sizeof(ctx->name) - 1);
  ctx->iface.name = ctx->name;
  ctx->iface.interface_data = &ctx->ifdata;
  ctx->iface.driver_data = ctx;
  ctx->ifdata.tx_func = csp_virtual_link_tx_frame;
  ctx->ifdata.max_frame_size = MAX_BYTES_IN_LINK_FRAME;

  /* Connect to the virtual link server and perform the initial handshake. */
  int error = csp_virtual_link_handshake(ctx);
  if (error != CSP_ERR_NONE) {
    csp_virtual_link_free(ctx);
    return error;
  }

  /* Add interface to CSP. */
  if ((error = v_csp_ground_link_add_interface(&ctx->iface)) != CSP_ERR_NONE) {
    csp_log_error("%s[%s]: v_csp_ground_link_add_interface() failed, error: %d",
                  __FUNCTION__, ctx->name, error);
    csp_virtual_link_free(ctx);
    return error;
  }

  /* Start receiving frames. */
  if (pthread_create(&ctx->rx_thread, NULL, csp_virtual_link_rx_thread, ctx)) {
    csp_log_error("%s[%s]: pthread_create() failed, error: %s", __FUNCTION__,
                  ctx->name, strerror(errno));
    /* We cannot free the interface because we cannot remove it from CSP.
       Yes, this is bad. */
    return CSP_ERR_NOMEM;
  }

  if (iface != NULL) {
    *iface = &ctx->iface;
  }

  return CSP_ERR_NONE;
}

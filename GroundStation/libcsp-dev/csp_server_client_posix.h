
#ifndef _CSP_SERVER_CLIENT_POSIX_DRIVER_H_
#define _CSP_SERVER_CLIENT_POSIX_DRIVER_H_


#include <csp/csp.h>
#include <csp/csp_debug.h>
#include <pthread.h>
#include "csp_server_client.h"

static int csp_pthread_create(void * (*routine)(void *));
static void * task_router(void * param);
static void * task_server(void * param);
static void * task_client(void * param);
int router_start(void);
int server_start(void);
int client_start(void);

#endif

#include "tcp.h"

int createServer(void){
    int serverfd;
   // int clientfd;
    struct sockaddr_in server_address;
    
    //struct sockaddr_in client;
    //socklen_t client_addr_size;
    
 /*Create Local Socket*/
  if((serverfd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
       handle_error("Error create socket\r\n");
    }
    /*Set IP and PORT*/
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(PORT);

    /*Bind Socket to IP*/\
    if((bind(serverfd,(struct sockaddr *)&server_address, sizeof(server_address))) != 0){
        handle_error("Error bind socket\r\n");
    }

    /*Set Server to Passively Listen*/
    if((listen(serverfd,BACKLOG_QUEUE_SIZE)) == -1){
        handle_error("Error listen\r\n");
    }
    
    
//     clientfd = accept(serverfd,(struct sockaddr *) &client, &client_addr_size);
//     if(clientfd == -1){
//         handle_error("Error accept\r\n");
//     }
    
return(serverfd);
}


int createClient(void){
    int clientfd;
    struct sockaddr_in client_address;
    
    /*Create Local Socket*/
  if((clientfd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
       handle_error("Error create socket\r\n");
       
    /*Set IP and PORT*/
    client_address.sin_family = AF_INET;
    client_address.sin_addr.s_addr = inet_addr("127.0.0.1");
    client_address.sin_port = htons(PORT);
    
    if( (connect(clientfd, (struct sockaddr *)&client_address, sizeof(client_address)) == -1 ) ){
        handle_error("Error client connect\r\n");
    }
  }
  return(clientfd);
}



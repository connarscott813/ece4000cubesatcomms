#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "tcp.h"

int main(){
    struct sockaddr_in client;
    socklen_t client_addr_size;
    int connfd = 0;
    uint32_t i;
    char buffer[256];
    int serverfd = createServer();
    
    while(1){
     

        connfd = accept(serverfd,(struct sockaddr *) &client, &client_addr_size);
        if(connfd == -1){
            handle_error("Error accept\r\n");
            }
        
        if(connfd > 0){
            while(1){
                read(connfd, buffer, sizeof(buffer));
                
                printf("Read: %s\r\n", buffer);
                i = 0;
                while((buffer[i++] = getchar()) != '\n');
                
                write(connfd, buffer, sizeof(buffer));
            }
        }
            
        }
    
    
    return(0);
}

#ifndef _TCP_H_
#define _TCP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define PORT 8080
#define BACKLOG_QUEUE_SIZE 256
#define handle_error(msg) \
    do{perror(msg); exit(EXIT_FAILURE);}while(0)


int createServer(void);

int createClient(void);

#endif

#include "execs.h" 

static int rotctld_pid;
 
int start_rotctld(void){
    char * cmd[10] = {"rotctld", "-m", MD01_HAMLIBID, 
                    "-t", MD01_HAMLIBPORT, 
                    "-s", MD01_BAUDRATE, 
                    "-r", MD01_USBPORT, NULL};
    rotctld_pid = forkProc();
    if(rotctld_pid == 0){
        execute(cmd, 10);
        return(0);
    }
    else{
        return(rotctld_pid);
    }

}

void stop_rotctld(void){
    kill(rotctld_pid, SIGTERM);
}

void exec_scheduleAOS(void){
    char * cmd[] = {"./scheduleAOS/scheduleAOS", NULL};
    executeCmd(cmd, 2);
}

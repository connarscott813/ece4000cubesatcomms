/**
 * @file execs.h
 * @author Connar Scott
 * @brief functions for starting processes
 */

#ifndef _EXECS_H_
#define _EXECS_H_

#include "shellcmd.h"

#define MD01_HAMLIBID "903"
#define MD01_HAMLIBPORT "4533"
#define MD01_BAUDRATE "115200"
#define MD01_USBPORT "/dev/ttyUSB0"

/**
 * Starts the rotctld process
 * @return process ID of rotctld
 */
int start_rotctld(void);

/**
 * Stops the rotctld process
 * @return None
 */
void stop_rotctld(void);

/**
 * Executes the scheduleAOS executable
 * @return None
 */
void exec_scheduleAOS(void);

#endif

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>

#include "execs.h"

void sigint_handler(int sig_num){
    printf("\r\nProcess Ended\r\n");
    stop_rotctld();
    
    exit(0);
}


int main(){
    int i = 0;
    
    signal(SIGINT, sigint_handler);
    start_rotctld();
    
    /*Get Pass*/
    
    while(1){
     
        if(i >= 60){
         
            exec_scheduleAOS();
            i = 0;
            
        }
        
        sleep(60);
        i++;
        
    }
    

    stop_rotctld();
    
    return(0);
}

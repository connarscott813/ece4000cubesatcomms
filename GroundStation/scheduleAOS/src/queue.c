#include "queue.h"

void queueInit(queue_t * queue, int queueSize){
    queue->capacity = queueSize;
    queue->front = 0;
    queue->size = 0;
    queue->rear = queueSize - 1;
}

void enqueue(queue_t * queue, int data){

    if(isQFull(queue)){
        perror("Error: Queue is full");
        return;
    }

    queue->rear = (queue->rear + 1) % (queue->capacity);
    queue->array[queue->rear] = data;
    queue->size+=1;
}

int dequeue(queue_t * queue){
    if(isQEmpty(queue)){
        perror("Error: Queue is empty");
        exit(1);
    }

    int data = queue->array[queue->front];
    queue->front = (queue->front + 1) % (queue->capacity);
    queue->size-=1;
    return(data);
}

char queueCopy(queue_t * source, queue_t * dest){
    uint8_t i;
    
    dest->capacity = source->size;
    dest->front = 0;
    dest->size = 0;
    dest->rear = source->size - 1;
    if(isQEmpty(source)) return(0);
    
    int buffer[source->size];
    
    for(i = 0; !isQEmpty(source); i++){
        buffer[i] = dequeue(source);
    }
    
    for(i = 0; !isQFull(dest); i++){
        enqueue(dest, buffer[i]);
        enqueue(source, buffer[i]);
    }
    return(1);
}

void queue2array(queue_t * queue, unsigned char * buffer, uint32_t maxsize){
    int i;
    uint32_t temp;
    queue_t tempQ;
    
    queueCopy(queue, &tempQ);


    for(i = 0; !isQEmpty(&tempQ); i++){
        
        temp = (uint32_t)dequeue(&tempQ);
        buffer[i*4+3] = temp & 0xff;
        buffer[i*4+2] = (temp >> 8) & 0xff;
        buffer[i*4+1] = (temp >> 16) & 0xff;
        buffer[i*4+0] = (temp >> 24) & 0xff;
        }
}

uint8_t isQEmpty(queue_t * queue){
    return(queue->size == 0);
}

uint8_t isQFull(queue_t * queue){
    return(queue->size == queue->capacity);
}

char printQueue(queue_t * queue){

    queue_t queueTemp;
    queueTemp.capacity = queue->size;
    queueTemp.front = 0;
    queueTemp.size = 0;
    queueTemp.rear = queue->size - 1;
    if(isQEmpty(queue)) return(-1);

    int buffer[queue->size];

    printf("Queue:");

    for(uint8_t i = 0; !isQEmpty(queue); i++){
        buffer[i] = dequeue(queue);
        printf("%d ", buffer[i]);
        enqueue(&queueTemp, buffer[i]);
    }
    
    printf("\n");

    //put back into queue
     for(uint8_t i = 0; !isQEmpty(&queueTemp); i++){
        enqueue(queue, dequeue(&queueTemp));
    }
    return(1);
}

void queueRemove(queue_t * queue, uint32_t val){
    uint32_t i;
    uint32_t tempSize = queue->size;
    uint32_t buffer[tempSize];
    
    for(i = 0; !isQEmpty(queue); i++){
        buffer[i] = dequeue(queue);
    }
    
    for(i = 0; i < tempSize; i++){
        if(!(buffer[i] == val))
            enqueue(queue, buffer[i]);
    }
}


void queueInitString(string_queue_t * queue, int queueSize){
    if(queueSize > 64){
        printf("Error input queueSize too large\r\n");
        return;
    }
    queue->capacity = queueSize;
    queue->front = 0;
    queue->size = 0;
    queue->rear = queueSize - 1;
}

void enqueueString(string_queue_t * queue, char * string){

    if(isStringQFull(queue)){
        perror("Error: Queue is full");
        return;
    }

    queue->rear = (queue->rear + 1) % (queue->capacity);
    strcpy(queue->array[queue->rear],string);
    queue->size+=1;
}

void dequeueString(string_queue_t * queue, char * output){
    if(isStringQEmpty(queue)){
        perror("Error: Queue is empty");
        exit(1);
    }
    strcpy(output,queue->array[queue->front]);
    queue->front = (queue->front + 1) % (queue->capacity);
    queue->size-=1;
}

uint8_t isStringQEmpty(string_queue_t * queue){
    return(queue->size == 0);
}

uint8_t isStringQFull(string_queue_t * queue){
    return(queue->size == queue->capacity);
}


#include "atd_wraps.h"

/**
 * Organizes AOS into syntax for atd
 * @param next_AOS - aos structure
 * @param next_LOS - los structure
 * @param atd_buffer - array of strings for storing atd syntax
 * @param atd_buffer_size - size of atd buffer
 * @return None
 */
void aos_to_atsyntax(aos_t next_AOS, los_t next_LOS, char ** atd_buffer, uint32_t atd_buffer_size){
    char  date_buffer[STRING_BUFSIZE] = EMPTY_STRING; // XX-XX-XX
    char  time_buffer[STRING_BUFSIZE] = EMPTY_STRING; //XX:XX
    
    char* atd_cmd = "at";
    const char* date_delim = "-";
    const char* time_delim = ":";
    
    next_AOS.date.year[0] = '2';
    next_AOS.date.year[1] = '2';
    next_AOS.date.year[2] = '\0';

    next_AOS.date.month[0] = '0';
    next_AOS.date.month[1] = '3';
    next_AOS.date.month[2] = '\0';
    
    next_AOS.date.day[0] = '3';
    next_AOS.date.day[1] = '0';
    next_AOS.date.day[2] = '\0';
    
    next_AOS.time.hour[0] = '1';
    next_AOS.time.hour[1] = '5';
    next_AOS.time.hour[2] = '\0';

    next_AOS.time.minute[0] = '2';
    next_AOS.time.minute[1] = '7';
    next_AOS.time.minute[2] = '\0';
    
    next_AOS.time.second[0] = '0';
    next_AOS.time.second[1] = '0';
    next_AOS.time.second[2] = '\0';
    
    
    //organize date for atd
    strcat(date_buffer, next_AOS.date.year);
    strcat(date_buffer, date_delim);
    strcat(date_buffer, next_AOS.date.month);
    strcat(date_buffer, date_delim);
    strcat(date_buffer, next_AOS.date.day);

    //organize time for atd
    strcat(time_buffer, next_AOS.time.hour);
    strcat(time_buffer, time_delim);
    strcat(time_buffer, next_AOS.time.minute);

    //organize time & date for atd 
    if(atd_buffer_size < 3) atd_buffer[0] = NULL;
    else{
        strcpy(atd_buffer[0] ,atd_cmd);
        strcpy(atd_buffer[1] ,time_buffer);
        strcpy(atd_buffer[2] ,date_buffer);
    }
    
}

/**
 * Appends parameters to atd syntax from aos_to_atsyntax.
 * @param atd_buffer - array of strings for storing atd syntax
 * @param atd_buffer_size - size of atd buffer
 * @param atd_commands - commands to be sent to atd(array of strings)
 * @param atd_commands_size - size of atd commands
 * @return None
 */
void add_atdcommands(char ** atd_buffer, uint32_t atd_buffer_size, 
                    char ** atd_commands, uint32_t atd_commands_size){
    
    uint32_t i = 0;
    uint32_t atd_param_count = 0;
    
       if(DEBUG_ATDWRAP){
        uint32_t j = 0;
        printf("atd full: ");
        while(atd_buffer[j] != NULL){
            printf("%s ", atd_buffer[j]);
            j++;
        }
        printf("\n");
    }
    
    //get num parameters in initial atd_buffer
    while(strcmp(atd_buffer[atd_param_count], EMPTY_STRING)
        && atd_param_count < atd_buffer_size)
        {atd_param_count++;}

    //add atd commands to the atd buffer
    while(strcmp(atd_commands[i], EMPTY_STRING) 
            && i < atd_commands_size
            && (i+atd_param_count) < atd_buffer_size){
        
            strcpy(atd_buffer[i + atd_param_count],atd_commands[i]); //append commands to buffer
            i++;

    }
    
    if(i + atd_param_count < atd_buffer_size){ //buffer check
        atd_buffer[i + atd_param_count] = NULL; //append null to end
    }
    
    if(DEBUG_ATDWRAP){
        uint32_t j = 0;
        printf("atd full: ");
        while(atd_buffer[j] != NULL){
            printf("%s ", atd_buffer[j]);
            j++;
        }
        printf("\n");
    }
    
}

/**
 * Outputs current atd jobs to ATD_JOB_LIST_LOC
 * @return None
 */
void get_atdjobs(void){
    uint8_t atq_size = 4;
    char *atq[atq_size];
    atq[0] = "atq";
    atq[1] = ">";
    atq[2] = ATD_JOB_LIST_LOC;
    atq[3] = NULL;
    //check for/create file
    FILE * atqfile = fopen((const char *)ATD_JOB_LIST_LOC, "w");
    if(atqfile == NULL){
        printf("Error atqfile does not exist\n\r");
    }
    else{
        fclose(atqfile);

        if(DEBUG_ATDWRAP){
            uint8_t i = 0;
            while(atq[i] != NULL){
                printf("%s ",atq[i]);
                i++;
            }
            printf("\n\r");
        }
        
        //execute atq command
        executeCmd(atq, ATDQ_SIZE);
        }
}

/**
 * Deletes all jobs specified in the ATD_JOB_LIST_LOC location.
 * @return None
 */
void delete_atdjobs(void){
    const char * mode_read_file = "r";
    uint32_t i;
    
    char buffer[BUFFER_SIZE];
    char atdjob_num[BUFFER_SIZE];
    uint8_t atrm_size = 3;
    char ** atrm = (char**)calloc(atrm_size,sizeof(char*));
    
    for(i = 0;i < atrm_size; i++){
            atrm[i] = (char*)calloc(atrm_size,STRING_BUFSIZE*sizeof(char));
            }
    strcpy(atrm[0], "atrm");
    atrm[2] = NULL;


    FILE * atdjobfile = fopen((const char *)ATD_JOB_LIST_LOC, mode_read_file);
    if(atdjobfile == NULL){ printf("%s does not exists\n\r", ATD_JOB_LIST_LOC);}
    else{

        while(fgets(buffer, BUFFER_SIZE, atdjobfile) != NULL){
            //put job number into string
            i = 0;
            while(buffer[i] <= '9' && buffer[i] >= '0' ){   
                atdjob_num[i] = buffer[i];
                i++;
            }
            atdjob_num[i] = '\0';

            strcpy(atrm[1], atdjob_num);
            //delete job
            executeCmd(atrm, atrm_size);
        }

        fclose(atdjobfile);
       
        for(i = 0;i < atrm_size; i++){
            free(atrm[i]);
        }
        free(atrm);
    }
}

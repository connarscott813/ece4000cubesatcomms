/**
 * @file scheduleAOS.h
 * @author Connar Scott
 * @brief Functions for extracting AOS and LOS from
 *        a g_data_t structure
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifndef _SCHEDULEAOS_H_
#define _SCHEDULEAOS_H_

#define STRING_BUFSIZE 16
#define ATDCMD_SIZE 24
#define ATDBAGGAGE_SIZE 16
#define DEBUG_SCHEDULEAOS 0

#include "getPassInfo.h"

typedef struct aos{
  gtime_t time;
  gdate_t date;
}aos_t;

typedef struct los{
  gtime_t time;
  gdate_t date;
}los_t;

uint8_t getNextAOS(g_data_t gdataStruct[], uint32_t datasize, aos_t *next_AOS, los_t *next_LOS);
void printNextAOS(aos_t next_AOS, los_t next_LOS);
#endif

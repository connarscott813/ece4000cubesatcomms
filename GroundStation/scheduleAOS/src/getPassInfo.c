#include "getPassInfo.h" 

uint32_t readGpredictFile(void){
    
    FILE * gpredictdata;
    const char * mode_read_file = "r";
    
    //open file + check that file exists
    gpredictdata = fopen(GPREDICT_DATA_PATH, mode_read_file);
    
    if(gpredictdata == NULL){ perror("gpredictdata does not exists\n\r");}
    else{
        //get size of file
        uint32_t num_file_lines = getFileSize(gpredictdata);
        if(DEBUG_GPI)printf("# lines = %d\n\r", num_file_lines);
        //reopen file but make sure its closed first
        fclose(gpredictdata);
        
        return(num_file_lines - HEADER_SIZE);
    }
    return(0);
}


void organizeGpredictData(g_data_t gpredictStruct[], uint32_t datasize){
    
    FILE * gpredictdata;
    const char * mode_read_file = "r";
    
    gpredictdata = fopen(GPREDICT_DATA_PATH, mode_read_file);
        if(gpredictdata == NULL){perror("gpredictdata does not exists\n\r");}
        
        storeGpredictData(gpredictStruct, datasize, gpredictdata);
        
        fclose(gpredictdata);
}


uint32_t getFileSize(FILE * file){
    uint32_t lineCount = 0;
    char buffer[BUFFER_SIZE];
    
    while( fgets(buffer, BUFFER_SIZE, file) != NULL ) lineCount++;
        return(lineCount);
}


void storeGpredictData(g_data_t gpredictData[], uint32_t datasize, FILE * file){
    char buffer[BUFFER_SIZE];
    char * token_buffer[BUFFER_SIZE];
    
    const char * delim = "  ";
    const uint8_t headersize = 4;
    
    uint32_t index = 0; 
    uint32_t i;
    
    //move past header
//     while(i < headersize){
//         if(fgets(buffer, BUFFER_SIZE, file) == NULL)
//             perror("file not defined in storeGpredictData\n\r");
//         i++;
//     }
    
    //get line and tokenize based on delim
    while(fgets(buffer, BUFFER_SIZE, file) != NULL){
        
        if(i < headersize) i++;
        else{
            string2tokens(buffer, token_buffer, BUFFER_SIZE, delim);
            //check that token_buffer is defined
            if(token_buffer == NULL){perror("token_buffer not defined\n\r");}
            else{
                //Put data into g_data structure
                gpredictData[index].Az = atof(token_buffer[2]);
                gpredictData[index].El = atof(token_buffer[3]);
                set_gdate(gpredictData, datasize, index, token_buffer[0]);
                set_gtime(gpredictData, datasize, index, token_buffer[1]);

                index++;
            }
        }
    }
}


void set_gdate(g_data_t gpredictData[], uint32_t datasize, uint32_t index, char *token){
 
    gpredictData[index].gdate.year[0] = token[2];
    gpredictData[index].gdate.year[1] = token[3];
    gpredictData[index].gdate.year[2] = '\0';
    
    gpredictData[index].gdate.month[0] = token[5];
    gpredictData[index].gdate.month[1] = token[6];
    gpredictData[index].gdate.month[2] = '\0';
    
    gpredictData[index].gdate.day[0] = token[8];
    gpredictData[index].gdate.day[1] = token[9];
    gpredictData[index].gdate.day[2] = '\0';


}


void set_gtime(g_data_t gpredictData[], uint32_t datasize, uint32_t index, char *token){
 
    gpredictData[index].gtime.hour[0] = token[0];
    gpredictData[index].gtime.hour[1] = token[1];
    gpredictData[index].gtime.hour[2] = '\0';
    
    gpredictData[index].gtime.minute[0] = token[3];
    gpredictData[index].gtime.minute[1] = token[4];
    gpredictData[index].gtime.minute[2] = '\0';
    
    gpredictData[index].gtime.second[0] = token[6];
    gpredictData[index].gtime.second[1] = token[7];
    gpredictData[index].gtime.second[2] = '\0';


}


int string2tokens(char string[], char* tokenArray[], const int tokenArraySize, const char* delim ){
    int token_count = 0; //counter for number of current tokens
    char *saveptr;
    char *token;
    token = strtok_r(string, delim, &saveptr); //holds token

    while(token != NULL && token_count < tokenArraySize){
        tokenArray[token_count] = token;
        token = strtok_r(NULL, delim, &saveptr);
        token_count++;
    }

    tokenArray[token_count] = NULL; //make sure last value is a null
    return(token_count);

}


void printgdata(g_data_t gpredictData[], uint32_t datasize){
    uint32_t i;
    
    //header
    printf("DATE\t\tTIME\t\tAZ\t\tEL\r\n");
    
    for(i = 0; i < datasize-1; i++){
        //print date
        printf("%s-", gpredictData[i].gdate.year);
        printf("%s-", gpredictData[i].gdate.month);
        printf("%s\t", gpredictData[i].gdate.day);
        //print time
        printf("%s:", gpredictData[i].gtime.hour);
        printf("%s:", gpredictData[i].gtime.minute);
        printf("%s\t", gpredictData[i].gtime.second); 
        //print Azimuth information
        printf("%f\t", gpredictData[i].Az);
        //print Elevation information
        printf("%f\r\n", gpredictData[i].El);        
    }
    
}

/**
 * @file atd_wraps.h
 * @author Connar Scott
 * @brief Functions to perform and interact with the 
 *        unix shell command at
 * 
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifndef _ATDWRAPS_H_
#define _ATDWRAPS_H_

#define ATDCMD_SIZE 24
#define ATDBAGGAGE_SIZE 16
#define ATDQ_SIZE 4
#define STRING_BUFSIZE 16

#define DEBUG_ATDWRAP 0

#define EMPTY_STRING ""
#define ATD_FILE_OPTION "-f"
#define ATDCMD_FILE_LOC "atcmd.txt"
#define ATD_JOB_LIST_LOC "atdjobs.txt"


#include "scheduleAOS.h"
#include "shellcmd.h"

/**
 * Organizes AOS into syntax for atd
 * @param next_AOS - aos structure
 * @param next_LOS - los structure
 * @param atd_buffer - array of strings for storing atd syntax
 * @param atd_buffer_size - size of atd buffer
 * @return None
 */
void aos_to_atsyntax(aos_t next_AOS, los_t next_LOS,
                     char ** atd_buffer, uint32_t atd_buffer_size);

/**
 * Appends parameters to atd syntax from aos_to_atsyntax.
 * @param atd_buffer - array of strings for storing atd syntax
 * @param atd_buffer_size - size of atd buffer
 * @param atd_commands - commands to be sent to atd(array of strings)
 * @param atd_commands_size - size of atd commands
 * @return None
 */
void add_atdcommands(char ** atd_buffer, uint32_t atd_buffer_size,
                     char ** atd_commands, uint32_t atd_commands_size);
/**
 * Outputs current atd jobs to ATD_JOB_LIST_LOC
 * @return None
 */
void get_atdjobs(void);

/**
 * Deletes all jobs specified in the ATD_JOB_LIST_LOC location.
 * @return None
 */
void delete_atdjobs(void);
#endif

/**
 * @file queue.h
 * @author Connar Scott
 * @brief Functions to use queues
 */

#ifndef _QUEUE_H_
#define _QUEUE_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "config.h"
//#include "gs_reception.h"

#define MAX_STRQ_SIZE 64
#define ERROR_Q_SIZE 256

typedef struct queue_t {
    int front, rear;
    uint32_t size, capacity;
    int array[ERROR_Q_SIZE];

}queue_t;

typedef struct string_queue_t {
    int front, rear;
    uint32_t size, capacity;
    char array[64][128];

}string_queue_t;

/**
 * Initializes the queue_t structure
 * @param queue - pointer to queue_t structure
 * @param queueSize - max queue size
 * @return None
 */
void queueInit(queue_t * queue, int queueSize);

/**
 * Enqueues the queue with data
 * @param queue - pointer to queue structure
 * @param data - data to enqueue
 * @return None
 */
void enqueue(queue_t * queue, int data);

/**
 * Dequeues the queue
 * @return dequeued integer value
 */
int dequeue(queue_t * queue);

/**
 * Copies the queue from the source to destination
 * @param source - pointer to queue to copy from
 * @param dest - pointer to destination queue
 * @return False if source Q is empty, True otherwise
 */
char queueCopy(queue_t * source, queue_t * dest);

/**
 * Converts queue contents to a char array
 * @param queue - pointer to queue structure
 * @param buffer[] - array to store queue values
 * @param maxsize - the max size of the array
 * @return None
 */
void queue2array(queue_t * queue, unsigned char buffer[], uint32_t maxsize);

/**
 * Checks if queue is empty
 * @param queue - pointer to queue structure
 * @return True if queue is empty
 */
uint8_t isQEmpty(queue_t * queue);

/**
 * Checks if queue is full
 * @param queue - pointer to queue structure
 * @return True if queue is full
 */
uint8_t isQFull(queue_t * queue);

/**
 * prints the contents of the queue
 * @param queue - pointer to queue structure
 * @return returns -1 if queue is empty, otherwise 1
 */
char printQueue(queue_t * queue);

/**
 * Removes indicated value from the queue
 * @param queue - pointer to queue structure
 * @param val - value to remove from queue
 * @return None
 */
void queueRemove(queue_t * queue, uint32_t val);

/**
 * Initializes a queue to store strings
 * @param queue - pointer to string_queue structure
 * @param queueSize - max size of the queue
 * @return None
 */
void queueInitString(string_queue_t * queue, int queueSize);

/**
 * Enqueues the string queue
 * @param queue - pointer to string_queue structure
 * @param string - string to enquue
 * @return None
 */
void enqueueString(string_queue_t * queue, char * string);

/**
 * Dequeues the string queue
 * @param queue - pointer to string_queue structure
 * @param output - output string of the queue
 * @return None
 */
void dequeueString(string_queue_t * queue, char * output);

/**
 * Checks if the string queue is empty
 * @param queue - pointer to string_queue structure
 * @return True if empty
 */
uint8_t isStringQEmpty(string_queue_t * queue);

/**
 * Checks if the string queue is full
 * @param queue - pointer to string_queue structure
 * @return True if full
 */
uint8_t isStringQFull(string_queue_t * queue);

#endif

#include "scheduleAOS.h"

/**
 * Puts the next AOS and LOS into the next_AOS struct
 * @param gdataStruct[] - gpredict_data struct array
 * @param datasize - size of gpredict_data array
 * @param next_AOS - pointer to the aos structure
 * @return None
 */
uint8_t getNextAOS(g_data_t gdataStruct[], uint32_t datasize, aos_t *next_AOS, los_t *next_LOS){
    uint32_t i;
    uint8_t flag = 0;
    for(i = 0; i < datasize; i++){
        //aos
        if(!flag){
            if(gdataStruct[i].El > MIN_AOS_UHFVHF){
                strcpy(next_AOS->time.hour, gdataStruct[i].gtime.hour);
                strcpy(next_AOS->time.minute, gdataStruct[i].gtime.minute);
                strcpy(next_AOS->time.second, gdataStruct[i].gtime.second);
                
                strcpy(next_AOS->date.year, gdataStruct[i].gdate.year);
                strcpy(next_AOS->date.month, gdataStruct[i].gdate.month);
                strcpy(next_AOS->date.day, gdataStruct[i].gdate.day);
                flag = 1;
            }
        }
        //los
        else{
            if(gdataStruct[i].El > MIN_AOS_UHFVHF){
            strcpy(next_LOS->time.hour, gdataStruct[i].gtime.hour);
            strcpy(next_LOS->time.minute, gdataStruct[i].gtime.minute);
            strcpy(next_LOS->time.second, gdataStruct[i].gtime.second);  
            
            strcpy(next_LOS->date.year, gdataStruct[i].gdate.year);
            strcpy(next_LOS->date.month, gdataStruct[i].gdate.month);
            strcpy(next_LOS->date.day, gdataStruct[i].gdate.day); 
            }
        }
    }
    return(flag);
}

/**
 * Prints the contents of the aos_t structure
 * @param next_AOS - AOS structure
 * @return None
 */
void printNextAOS(aos_t next_AOS, los_t next_LOS){
    
        printf("AOS: %s-", next_AOS.date.year);
        printf("%s-", next_AOS.date.month);
        printf("%s\t", next_AOS.date.day);
        //print time
        printf("%s:", next_AOS.time.hour);
        printf("%s:", next_AOS.time.minute);
        printf("%s\n\r", next_AOS.time.second); 
        
        printf("LOS: %s-", next_LOS.date.year);
        printf("%s-", next_LOS.date.month);
        printf("%s\t", next_LOS.date.day);
        //print time
        printf("%s:", next_LOS.time.hour);
        printf("%s:", next_LOS.time.minute);
        printf("%s\n\r", next_LOS.time.second);    
        
}




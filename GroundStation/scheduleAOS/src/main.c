#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "getPassInfo.h"
#include "scheduleAOS.h"
#include "shellcmd.h"
#include "atd_wraps.h"
 
 
int main(){
    
   
    //check that file is defined and get num_entries in file
    uint32_t num_entries = readGpredictFile();
    if(!num_entries){return(0);}
    
    //create and fill structure for pass information
    g_data_t gpredictStruct[num_entries];
    organizeGpredictData(gpredictStruct, num_entries);
    
    //get next aos and los check that they're defined
    aos_t nextAOS; los_t nextLOS;
    if(!getNextAOS(gpredictStruct, num_entries, &nextAOS, &nextLOS)){
        printf("AOS not defined > %f degrees\n\r", MIN_AOS_UHFVHF);
        return(0);}
    
    //create and allocate memory for atd buffers
    char ** atd_cmd = (char**)calloc(ATDCMD_SIZE,sizeof(char*));
    char ** atd_baggage = (char**)calloc(ATDBAGGAGE_SIZE,sizeof(char*)); 
    
    uint32_t i = 0;
    for(i = 0;i < ATDCMD_SIZE; i++){
            atd_cmd[i] = (char*)calloc(ATDCMD_SIZE,STRING_BUFSIZE*sizeof(char));
            }
    for(i = 0;i < ATDBAGGAGE_SIZE; i++){
            atd_baggage[i] = (char*)calloc(ATDBAGGAGE_SIZE,STRING_BUFSIZE*sizeof(char));
            }
    
    //add parameters for atd
    strcpy(atd_baggage[0], ATD_FILE_OPTION);
    strcpy(atd_baggage[1], ATDCMD_FILE_LOC);

    printNextAOS(nextAOS, nextLOS);
    //delete other atd jobs
    get_atdjobs();
    delete_atdjobs();
    
    //form atd command for scheduling
    aos_to_atsyntax(nextAOS, nextLOS, atd_cmd, ATDCMD_SIZE);
    add_atdcommands(atd_cmd, ATDCMD_SIZE, atd_baggage, ATDBAGGAGE_SIZE);
    
    
    executeCmd(atd_cmd, ATDCMD_SIZE);

    //free allocated memory
    for(i = 0;i < ATDBAGGAGE_SIZE; i++){
        free(atd_baggage[i]);
        }
    for(i = 0;i < ATDCMD_SIZE; i++){
        free(atd_cmd[i]);
    }
    free(atd_baggage);
    free(atd_cmd);
        
    return(0);
    
}

/**
 * @file timecmpr.h
 * @author Connar Scott
 * @brief Functions to ge the time indicated by the system, and
 *        compare it with an inputted time
 */

#ifndef _TIMECMPR_H_
#define _TIMECMPR_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>

#include "getPassInfo.h"
#include "config.h"

#define DEBUG_TIMECMPR 0

/**
 * Updates the internal system time variable to the current time
 * @return None
 */
void getSysTime(void);

/**
 * Initializes one shot timer
 * @param timer - pointer to timer_t structure
 * @return None
 */
void oneShotTimerInit(timer_t * timer);

/**
 * Sets and starts one shot timer
 * @param timer - pointer to timer_t structure
 * @param time_ns - time in ns to time for
 * @return None
 */
void oneShotTimer(timer_t timer, long time_ns);

/**
 * Checks to see if timer has stopped
 * @param timer - pointer to timer_t structure
 * @return True if time has ended
 */
int oneShotTimerEnd(timer_t timer);

/**
 * Checks to see if timer has stopped
 * @param timer - pointer to timer_t structure
 * @param cmprTime - input time to compare with
 * @param numBits - number of bits sent
 * @return True if time has ended
 */
int printOneShotTime(timer_t timer, long cmprTime, uint32_t numBits);

/**
 * Returns 1 if the system time is greater than the input time
 * @param passTime - pointer to a gtime_t structure
 * @return boolean value 1 if system time > pass Time
 */
bool greaterThanSysTime(gtime_t * passTime);

/**
 * Prints the internal system time variable contents
 * @return None
 */
void printSysTime(void);

/**
 * Prints the internal system time variable contents and the 
 * contents of passTime
 * @return None
 */
void printCmprTime(gtime_t * passTime);

/**
 * Sets input variable gtime_t to system time
 * @param gtime - sets the gtime_t structure to be the current 
 *                time given by sys time
 * @return None
 */
void setGtimeToSysTime(gtime_t * gtime);

/**
 * Increments the gtime to be one second above prev_time
 * @param gtime - pointer to gtimet structure
 * @param prev_time - pointer to previous gtime_t structure
 * @return None
 */
void changeGtime(gtime_t * gtime, gtime_t * prev_time);

#endif

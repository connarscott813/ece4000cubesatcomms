/**
 * @file stateMachines.h
 * @author Connar Scott, Brett Robertson, Ethan Brewer
 * @brief State machines for demonstrating communication links
 */

#ifndef _STATE_MACHINES_H_
#define _STATE_MACHINES_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "shellcmd.h"
#include "getPassInfo.h"
#include "procs.h"
#include "config.h"
#include "queue.h"
#include "frames.h"
#include "file_util.h"
#include "gs_reception.h"
#include "gs_transmission.h"
#include "sys/time.h"

#define GNURADIO_STARTUP_TIME 4
#define GNURADIO_STOP_TIME 1
#define ZEROMQ_WRITE_TIME MSEC_TO_NSEC(200)

typedef enum linkMode{
        TX,
        RX
}linkMode_E;

typedef enum txrxState{
    GET_WINDOW,
    GET_ERRORS
}txrxState_E;

/**
 * S-Band state machine
 * @param elevation - elevation value 
 * @param processId - poitner to processId_t structure
 * @param uplinkCmdQ - pointer to string queue containing
 *                     uplink commmands
 * @return None
 */
void manageSBand(float elevation, processId_t processId, string_queue_t * uplinkCmdQ);

/**
 * S-Band state machine
 * @param direwolf_fd - direwolf file descriptor
 * @param uplinkCmdQ - pointer to string queue containing
 *                     uplink commmands
 * @return None
 */
void manageUHFVHF(int direwolf_fd, string_queue_t * uplinkCmdQ);


#endif

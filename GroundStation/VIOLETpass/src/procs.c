#include "procs.h"

#define GNURADIO_CMD_SIZE 2

static int rigctld_pid;

void gnuradio_startrx(void){
    char * rx[GNURADIO_CMD_SIZE] = {GNURADIO_RX, NULL};
    execute(rx, GNURADIO_CMD_SIZE);
}
void  gnuradio_stoprx(pid_t rx_pid){
    kill(rx_pid, SIGINT); 
}

void gnuradio_starttx(uint32_t frameSize){
    char frameSizeStr[4];
    sprintf(frameSizeStr, "%d", frameSize);
    char * tx[GNURADIO_CMD_SIZE+1] = {GNURADIO_TX, frameSizeStr, NULL};
    execute(tx, GNURADIO_CMD_SIZE+1);
}

void gnuradio_stoptx(pid_t tx_pid){
    kill(tx_pid, SIGINT); 
}

void startDirewolf(void) {
    char * dw[GNURADIO_CMD_SIZE] = {"direwolf", NULL};
    execute(dw, GNURADIO_CMD_SIZE);
}

void  stopDirewolf(pid_t dw_pid){
    kill(dw_pid, SIGINT); 
}

 
int start_rigctld(void){
    char * cmd[10] = {"rigctld", "-m", "3081", 
                      "-r", "/dev/ttyUSB1", NULL};
    rigctld_pid = forkProc();
    if(rigctld_pid == 0){
        execute(cmd, 10);
        return(0);
    }
    else{
        return(rigctld_pid);
    }

}

void stop_rigctld(void){
    kill(rigctld_pid, SIGTERM);
}









/*
 * frames.c
 * desc: main function for frame manipulation on VIOLET S-Band
 *  Created on: Feb. 9, 2022
 *      Author: Brett Robertson
 */
#include"frames.h"

/*frames and packets implemented as char arrays*/

static unsigned int pscn_ref=1, pscn=1;


int keyset=0; //Define (de)construction variables

//int len=0, len2;
unsigned char testkey[8]={0x70, 0x62, 0x53, 0x53, 0x57, 0x4f, 0x52, 0x44};

    WORD key_schedule[60] = {0};
    const BYTE aeskey[32]={26};
    const BYTE aes_iv[AES_BLOCK_SIZE]={0xf0,0xf1,0xf2,0xf3,0xf4,0xf5,0xf6,0xf7,0xf8,0xf9,0xfa,0xfb,0xfc,0xfd,0xfe,0xff};
    
void initKey(void){
     aes_key_setup(aeskey, &key_schedule, 128);   
}


void increment_pscn(){
    pscn++;
}

void set_pscn(uint32_t val){
 pscn = val;   
}

uint32_t return_pscn(){
 return(pscn);   
}

void fill_packet(unsigned char *packet, int length){

	while (length>=0){
		packet[length]= 0x75;
	length -= 1;
	}
	return;
}

//msgnode listCreatenode(unsigned int psf, unsigned int pscn, unsigned char msg[], unsigned int len){
//	unsigned int i;
//	msgnode newnode;
//	newnode.msgpsf=psf;
//	newnode.msgpscn=pscn;
//	for(i=0;i<len;i++){
//		newnode.message[i]=msg[i];
//	}
//	newnode.next=NULL;
//
//	return newnode;
//}
//
//void listInsert(msgnode insertnode, msgnode placementnode){
//	insertnode.next=placementnode.next;
//	placementnode.next=&insertnode;
//}
//
//unsigned int listMeasure(msgnode startnode){
//	unsigned int bigness = 0;
//	msgnode temp;
//	temp = startnode;
//	while(temp.next!=NULL){
//		bigness++;
//		&temp=temp.next;
//	}
//	return bigness;
//}

//void increment_pscn(){
//	int i, bigness = 4;
//
//	for (i=bigness-1;i>=0;i--){
//		if (pscn[i]<0xff){
//			pscn[i] += 1;
//			return;
//		}
//		else {
//			pscn[i]= 0x00;
//		}
//	}
//
//	for(i=0;i<bigness;i++){
//		pscn[i]=0; //reset pscn if it ever fills
//	}
//
//	return;
//}

unsigned int do_crc32(unsigned char *msg, unsigned int length){

	unsigned int errcode, currbyte, polygen = 0x04c11db7, mask; //using 'normal' crc32 polynomial divisor

	   int i, j;

	   i = 0;
	   errcode = 0xFFFFFFFF;
	   for (i=length-1;i>=0;i--) {
	      currbyte = msg[i];            // Get next byte.
	      errcode = errcode ^ currbyte;
	      for (j = 7; j >= 0; j--) {    // Do eight times.
	         mask = -(errcode & 1);
	         errcode = (errcode >> 1) ^ (polygen & mask);
	      }
	   }
	   errcode = errcode ^ 0xffffffff;  //complement

	   /* reference stackoverflow:
	    *  "https://stackoverflow.com/questions/21001659/crc32-algorithm-implementation-in-c-without-a-look-up-table-and-with-a-public-li"
	    * for crc32 algorithm without lookup table
	    */

	   return errcode;
	}

void do_hmac_hash(unsigned char *msg, unsigned char *hashout, unsigned int len){
	//unsigned int auth_tag[2];
	/* perform hmac encryption and return tag */

	SHA256_CTX ctx;
//	unsigned char hashout[64];

	sha256_init(&ctx);
	sha256_update(&ctx, msg, len);
	sha256_final(&ctx, hashout);

	return;
}

void do_hmac_encrypt(unsigned char *msg, unsigned char *hmac, unsigned char *key, unsigned int len){
	unsigned char ipad = 0x36, opad = 0x5c, hash1[len+8], hash2[40]={0};
	//hash1 has room for message and k+ appendment, hash2 has room for 32 byte hash1 output and k- appendment, hashout stores hash outputs
	unsigned char keypad[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	int i;

	for(i=0;i<(len+8);i++){
		hash1[i]=0;  //init to 0
	}

	for(i=0;i<8;i++){
		keypad[i]=keypad[i]|key[i]; //keypad is equivalent to padded key
	}

	for(i=0;i<8;i++){
		hash1[i]=ipad^keypad[i]; //k+ placed at start of hash1
		hash2[i]=opad^keypad[i]; //k- placed at start of hash2
	}

	for(i=0;i<len;i++){
		hash1[i+8]=msg[i]; //fill remainder of hash1 with msg
	}

	do_hmac_hash(&hash1[0], &hmac[0], len); //perform 1st hash

	for(i=0;i<32;i++){
		hash2[i+8]=hmac[i]; //fill remainder of hash2 with hash1 output
	}

	do_hmac_hash(&hash2[0], &hmac[0], 40); //hmac is finalized output

	return;
}

unsigned int frame_decon(unsigned char *frame, unsigned int len, int crypt, deconError_E error){
	printf("Commencing Frame De-Construction \n");

	unsigned char header[6], trailer[12], authcodearr[32]={0}, temparr[len-18], psf; //authcheck initialized to 1
	unsigned int errcode, errcodetrail, authcheck=1;
	int i;
   
    len = len-18; //correct input length for packet vs. frame
    
    if (crypt==1){
        aes_decrypt_ctr(frame, len+18, frame, key_schedule, 128, aes_iv);
    }
       // printf("post-decrypt frame:\n"); //printing these as strings is trash
        
//     for (i=0;i<(len+18);i++){
//        
//     printf("%x\n", frame[i]);
//     }
    

    
	for(i=0;i<6;i++){
		header[i]=frame[i];  //extract header
	}

	for(i=0;i<len;i++){
		temparr[i] = frame[i+6]; //extract message and write to linked list
	}

	psf=header[0] & 0x03;

	//printf("that broke it\n");

	for(i=0;i<12;i++){
		trailer[i]=frame[i+len+6];
	}

	errcode = do_crc32(&frame[0], len+6);

	for (i=0;i<4;i++){
		errcodetrail = errcodetrail << 8; //trail 0,1,2,3 in that order are crc32 error code
		errcodetrail = errcodetrail | trailer[i];
		}

	do_hmac_encrypt(temparr, &authcodearr[0], &testkey[0], len);

	for(i=0;i<8;i++){
		authcheck = authcheck & (authcodearr[i]==trailer[i+4]);
	}

//	for (i=0;i<4;i++){
//			pscn_return = pscn_return << 8;
//			pscn_return = pscn_return | pscn[i];
//			}
//	printf("pscn: %u \n", pscn_return); //pscn return is unsigned int form of pscn

//	increment_pscn();

	if ((errcode != errcodetrail)|(authcheck!=1)){
		printf("verification failed \n");
        pscn++;
		return pscn-1;
	} else{
		printf("verification successful \n");
        printf("temparr is: ");
		for(i=0;i<len;i++){
			windowmsg[(pscn - pscn_ref)].msg[i]=temparr[i];
            printf("%c", temparr[i]);
		}
		printf("\n");
        

        
        if(pscn - pscn_ref > WINDOW_LENGTH){
            pscn_ref = pscn;
        }
		windowmsg[(pscn - pscn_ref)].psf=psf;
		windowmsg[(pscn - pscn_ref)].pscn=pscn;
        printf("pscn is %d, pscnref is %d\n", pscn, pscn_ref);
        
        pscn_return = (header[2] << 0)|
                      (header[3] << 8)|
                      (header[4] << 16)|
                      (header[5] << 24);
        
        if(error == NOT_ERROR_FRAME){
            pscn++;
        }
	//	printf("this works\n");
		return 0;
	}

}



unsigned int frame_decon_vhf(unsigned char *frame, unsigned int len, int crypt, deconError_E error){
	printf("Commencing Frame De-Construction \n");

    

	unsigned char header[6], trailer[12], authcodearr[32]={0}, temparr[len-18], psf; //authcheck initialized to 1
	unsigned int errcode, errcodetrail, authcheck=1;
	int i;
   
    len = len-18; //correct input length for packet vs. frame
    
    if (crypt==1){
        aes_decrypt_ctr(frame, len+18, frame, key_schedule, 128, aes_iv);
    }
       // printf("post-decrypt frame:\n"); //printing these as strings is trash
        
//     for (i=0;i<(len+18);i++){
//        
//     printf("%x\n", frame[i]);
//     }
    

    
	for(i=0;i<6;i++){
		header[i]=frame[i];  //extract header
	}

	for(i=0;i<len;i++){
		temparr[i] = frame[i+6]; //extract message and write to linked list
	}

	psf=header[0] & 0x03;

	//printf("that broke it\n");

	for(i=0;i<12;i++){
		trailer[i]=frame[i+len+6];
	}

	errcode = do_crc32(&frame[0], len+6);

	for (i=0;i<4;i++){
		errcodetrail = errcodetrail << 8; //trail 0,1,2,3 in that order are crc32 error code
		errcodetrail = errcodetrail | trailer[i];
		}

	do_hmac_encrypt(temparr, &authcodearr[0], &testkey[0], len);

	for(i=0;i<8;i++){
		authcheck = authcheck & (authcodearr[i]==trailer[i+4]);
	}

//	for (i=0;i<4;i++){
//			pscn_return = pscn_return << 8;
//			pscn_return = pscn_return | pscn[i];
//			}
//	printf("pscn: %u \n", pscn_return); //pscn return is unsigned int form of pscn

//	increment_pscn();

	if ((errcode != errcodetrail)|(authcheck!=1)){
		printf("verification failed \n");
        pscn++;
		return pscn-1;
	} else{
		printf("verification successful \n");
        printf("temparr is: ");
		               for(i=0;i<len;i++){
			vhfmsg[i]=temparr[i];
            printf("%c", temparr[i]);
            }
		printf("\n");
        
        pscn_return = (header[2] << 0)|
                      (header[3] << 8)|
                      (header[4] << 16)|
                      (header[5] << 24);
        
        if(error == NOT_ERROR_FRAME){
           // pscn++;
        }
	//	printf("this works\n");
		return 0;
	}

}








uint32_t returnPSCN(void){
 return(pscn_return);   
}

unsigned int frame_con(unsigned char *packet, unsigned char *frame, unsigned int len, int crypt, unsigned char fvnin, unsigned char aidin, unsigned psfin){
	printf("Commencing Frame Construction \n");

	unsigned char header[6], trailer[12], authcodearr[32], pscnarr[4];
	unsigned int errcode, pscn_temp;
	int i;

    //printf("packet is:");
    
	for(i=0;i<32;i++){
		authcodearr[i]=0;
	}

	pscn_temp=pscn;
	for (i=3;i>=0;i--){
		pscnarr[i]=pscn_temp & 0xff;
		pscn_temp = pscn_temp >> 8;
	}

	header[0] = ((((fvnin & 0x03) << 4) | (aidin & 0x0f)) << 2) | (psfin & 0x03); // header[0] is fvn(2bit),aid(4bit),psf(2bit)
	header[1] = len; //header 1 is 8-bit length variable
	header[2] = pscnarr[0];
	header[3] = pscnarr[1];
	header[4] = pscnarr[2];
	header[5] = pscnarr[3]; //header 2-4 are pscn

	  do_hmac_encrypt(&packet[0], &authcodearr[0], testkey, len);

    for(i=4;i<12;i++){
    	trailer[i]=authcodearr[i-4];
    }

	for(i=0;i<6;i++){
		frame[i]=header[i];  //prepend header
	}

	for(i=0;i<len;i++){
		frame[i+6] = packet[i]; //insert message
	}

	errcode = do_crc32(&frame[0], len+6);

	for (i=3;i>=0;i--){
		trailer[i] = errcode & 0x000000ff;
		errcode = errcode >> 8; //trail 0,1,2,3 in that order are crc32 error code
	}

	for(i=0;i<12;i++){
			frame[i+len+6]=trailer[i];
		}
    printf("pre-encrypt frame: ");
    for (i=0;i<(len+18);i++){
      
    printf("%x", frame[i]);
    }
		printf("\r\n");
        
     //   printf("%u is the key sched\n", key_schedule); 
		if(crypt== 1){
		aes_encrypt_ctr(frame, len+18, frame, key_schedule, 128, aes_iv);
        }
        
        
        
    printf("post-encrypt frame: "); //printing these as strings is trash
    for (i=0;i<(len+18);i++){
        printf("%x", frame[i]);
    }
    printf("\r\n");

//     printf("%d dec len", len+18);
//     printf("%u is the key sched\n", key_schedule); 
   // aes_decrypt_ctr(frame, len+18, frame, &key_schedule, 128, aes_iv);

//     unsigned char postamble[] = "orphn";
//     
//     for (i=0;i<5;i++){
//         frame[len+18+i] = postamble[i];
//     }
    
    printArray(aes_iv, 16);
    //    printf("post-encrypt-amble frame: %s", frame);
	return 0; //edit or write target frame
}

//unsigned int dll_process(const char* src, const char* dest, int op/*, unsigned char psf, unsigned char aid, unsigned char fvn*/, unsigned int length, int crypt){  //commented out inputs are to be added later
//	int length2= (length + 18 - 36*op);
//	unsigned char input[length], output[length2]; //input is packet and output is frame for op = 0, opposite for op = 1
//	unsigned int statint;
//	int i; //for testing
//
//	if(keyset==0){
//        aes_key_setup(aeskey, &key_schedule, 128);
//        keyset++;
//    }
//
//	switch (op){
//	case 0: //set op to 0 if 'src' contains a packet to output a constructed frame to 'dest'
//
//		storeData(input, length, src); //read source to packet
//
//		statint = frame_con(&input[0], &output[0], length, crypt); //construct packet and store in frame
//
//		writeData(output, length2, dest); //write frame to destination
//		break;
//	case 1: //set op to 0 to deconstruct a frame from 'src' to 'dest'
//
//		storeData(input, length, src); //read source to frame
//		statint = frame_decon(&input[0], &output[0], length, crypt); //deconstruct frame and store in packet
//		writeData(output, length2, dest); //write packet to destination
//
//		break;
//	default:
//		printf("Invalid opcode\n");
//	}
//	return statint;
//
//}



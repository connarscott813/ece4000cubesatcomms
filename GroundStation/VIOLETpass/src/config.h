#ifndef _CONFIG_H_
#define _CONFIG_H_

/*File Paths*/
#define GPREDICT_DATA_PATH (const char*) "/data/gpredict_next_pass_test.txt"

#define RAMDISK_WINDOW_FOLDER_DEST "/media/ramdisk"

#define DISK_WINDOW_FOLDER_DEST "/data"

#define UPLINK_CMDS_DEST "/home/eceuser/ece4000cubesatcomms/GroundStation/VIOLETpass/bin/uplink_commands.bin"


#define GNURADIO_RX "/home/eceuser/ece4000cubesatcomms/GroundStation/VIOLETpass/bin/rx_gs_gnuradio.py"
#define GNURADIO_TX "/home/eceuser/ece4000cubesatcomms/GroundStation/VIOLETpass/bin/tx_gs_gnuradio.py"

#define GNURADIO_BIN_INPUT (const char*) "sample_airinterface"
#define GNURADIO_BIN_AIRINTERFACE (const char*) "/media/ramdisk/sample_airinterface.bin"
#define GNURADIO_BIN_OUTPUT (const char*) "/home/eceuser/ece4000cubesatcomms/SDRDevelopment/Ground-SDR-v1/Rx_Downlink_dev/downlink_rx.bin"

/*Project Parameters */
#define EN_SBAND 0
#define EN_UHFVHF 1

#define MIN_AOS_UHFVHF (float)10.0f
#define MIN_AOS_SBAND (float)25.0f

#define SBAND_HALFDUPLEX_DELAY (uint32_t)SEC_TO_NSEC(0.5)

#define FRAME_SIZE 160 //Bytes

#define ERROR_Q_SIZE 256

#define DEFAULT_UPLINK_CMD "Give me packets or else"
#define NO_ERROR_UPLINK "00000"

/*General Parameters*/

#define SEC_TO_NSEC(seconds) (long)(1000000000*seconds) 
#define MSEC_TO_NSEC(mseconds) (uint32_t)(1000*mseconds) 

#define FVN_DEFAULT 0x00
#define AID_DEFAULT 0x00
#define PSF_DEFAULT 0x03




#endif

#include "timecmpr.h"

static struct tm * systemTime;

void getSysTime(void){
    time_t currentTime = time(NULL);
    
    //systemTime = localtime(time);    
    systemTime = localtime(&currentTime);
    if(DEBUG_TIMECMPR) printSysTime();

}

void oneShotTimerInit(timer_t * timer){
    struct sigevent sev;
    
    sev.sigev_notify = SIGEV_NONE;
    if( (timer_create(CLOCK_REALTIME ,&sev ,timer) == -1)){
        printf("Error timer_create\r\n");   
    }
}

void oneShotTimer(timer_t timer, long time_ns){
    struct itimerspec itime;
    
    /*Set Timer Structure*/
    //repeat period zero
    itime.it_interval.tv_sec = 0;
    itime.it_interval.tv_nsec = 0;
    //set stop time
    itime.it_value.tv_sec = 2;
    itime.it_value.tv_nsec = 0;
        
    if(timer_settime(timer, 0, &itime, NULL) == -1){
        perror("Error timer_settime\r\n");
    }
}

int oneShotTimerEnd(timer_t timer){
    struct itimerspec itime;
    if(timer_gettime(timer, &itime) == -1){
        perror("Error timer_gettime\r\n");
    }
    printf("Timer time = %ld %ld\r\n", itime.it_value.tv_sec, itime.it_value.tv_nsec);
    return(itime.it_value.tv_nsec == 0);
}

int printOneShotTime(timer_t timer, long cmprTime, uint32_t numBits){
    struct itimerspec itime;
    if(timer_gettime(timer, &itime) == -1){
        perror("Error timer_gettime\r\n");
    }
    
    long bitrate = (((long)numBits )/(((long)0 + (long)2000000000) - ((long)itime.it_value.tv_sec*1000000000 +itime.it_value.tv_nsec)))/(long)1000000000;
    
    printf("BITRATE = %lu bits/sec\r\n", bitrate);
    printf("cmprtime = %lu \r\n", cmprTime);
    printf("timerval = %lu\r\n", ((long)itime.it_value.tv_sec*1000000000 +itime.it_value.tv_nsec));
    printf("Numbits = %u\r\n", numBits);
    return(itime.it_value.tv_nsec == 0);
}

bool greaterThanSysTime(gtime_t * passTime){
    bool sysTimeGreater = 0;
    
    getSysTime();
    
    if(DEBUG_TIMECMPR) printCmprTime(passTime);
    
    if((systemTime->tm_hour) > (atoi(passTime->hour))){
        sysTimeGreater = 1;}
    else if((systemTime->tm_hour) < (atoi(passTime->hour))){
        sysTimeGreater = 0;}
    else{
        if((systemTime->tm_min) > (atoi(passTime->minute))){
            sysTimeGreater = 1;}
        else if((systemTime->tm_min) < (atoi(passTime->minute))){
            sysTimeGreater = 0;}
        else{
         if((systemTime->tm_sec) > (atoi(passTime->second))){
             sysTimeGreater = 1;}
        else if((systemTime->tm_sec) < (atoi(passTime->second))){
            sysTimeGreater = 0;}
        }
    }
    
    return(sysTimeGreater);
    
}

void printSysTime(void){
    
    printf("SYSTEM TIME VAR\n\r");
    printf("sec   : %d\r\n", systemTime->tm_sec );
    printf("min   : %d\r\n", systemTime->tm_min );
    printf("hour  : %d\r\n", systemTime->tm_hour);
    printf("day   : %d\r\n", systemTime->tm_mday);
    printf("month : %d\r\n", systemTime->tm_mon );
    printf("year  : %d\r\n", systemTime->tm_year);
    
}

void printCmprTime(gtime_t * passTime){
    printf("\tSYS\tGTIME\n\r");
    printf("hour  : %d\t%d\r\n", systemTime->tm_hour, atoi(passTime->hour));
    printf("min   : %d\t%d\r\n", systemTime->tm_min, atoi(passTime->minute));
    printf("sec   : %d\t%d\r\n", systemTime->tm_sec, atoi(passTime->second));
   
}

void setGtimeToSysTime(gtime_t * gtime){
    char temp[3];
    int second, minute, hour, i;
    
    hour = systemTime->tm_hour;
    sprintf(temp, "%d", hour);
    for(i = 0; i < 3; i++)
{ gtime->hour[i] = temp[i];
}
    
    minute = systemTime->tm_min;
    sprintf(temp, "%d", minute);
    for(i = 0; i < 3; i++)
{ gtime->minute[i] = temp[i];
}
    
    second = systemTime->tm_sec;
    sprintf(temp, "%d", second);
    for(i = 0; i < 3; i++)
{ gtime->second[i] = temp[i];
}
}

void changeGtime(gtime_t * gtime, gtime_t * prev_time){
    char temp[3];
    int secondint, minuteint, hourint, i, minstart, secstart;
      //printf("IN1:%s %s %s\r\n", gtime->hour, gtime->minute,gtime->second);
      
      for(i = 0; i < 3; i++)
{
          gtime->hour[i] = prev_time->hour[i];
          gtime->minute[i] = prev_time->minute[i];
          gtime->second[i] = prev_time->second[i];
      }
      //printf("IN2:%s %s %s\r\n", gtime->hour, gtime->minute,gtime->second);
    
    if(atoi(gtime->second)+1 > 59){
        secondint = 0;
        sprintf(temp, "%d", secondint);
        if(secondint > 9){secstart = 0;} else{secstart = 1; gtime->second[0] = '0';}
        for(i = secstart; i < 3; i++)
{ gtime->second[i] = temp[i-secstart];
}
        printf("%s\r\n", gtime->second);

        if(atoi(gtime->minute)+1 > 59){
            minuteint = 0;
            sprintf(temp, "%d", minuteint);
            if(minuteint > 9){minstart = 0;} else{minstart = 1; gtime->minute[0] = '0';} 
            for(i = minstart; i < 3; i++)
{ gtime->minute[i] = temp[i-minstart];
}

            hourint = atoi(gtime->hour) + 1;
            sprintf(temp, "%d", hourint);
            for(i = 0; i < 3; i++)
{ gtime->hour[i] = temp[i];
}            }
        else{
            minuteint = atoi(gtime->minute) + 1;
            sprintf(temp, "%d", minuteint);
            if(minuteint > 9){minstart = 0;} else{minstart = 1; gtime->minute[0] = '0';}  
            for(i = minstart; i < 3; i++)
{ gtime->minute[i] = temp[i-minstart];
}
            }
    }
    else{
        secondint = atoi(gtime->second) + 1;
        sprintf(temp, "%d", secondint);
        if(secondint > 9){secstart = 0;} else{secstart = 1; gtime->second[0] = '0';} 
        for(i = secstart; i < 3; i++)
{ gtime->second[i] = temp[i-secstart];
}
    
    }
    //printf("%s %s %s\r\n", gtime->hour, gtime->minute,gtime->second);
    
}

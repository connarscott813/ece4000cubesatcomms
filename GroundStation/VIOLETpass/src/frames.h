/*
 * frames.h
 *  description: header for main frame functions source code
 *  Created on: Feb. 16, 2022
 *      Author: Brett Robertson
 */

#ifndef FRAMES_H_
#define FRAMES_H_

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdint.h>
#include"sha256.h"
// #include"file_util.h"
#include"aes.h"
#include"config.h"
#include "gs_reception.h"
// #include <openssl/aes.h>

#define WINDOW_LENGTH 10
#define DLL_OVERHEAD 18
#define MAX_PACKET_LENGTH 255

typedef enum deconError{
    ERROR_FRAME,
    NOT_ERROR_FRAME
}deconError_E;

//typedef struct msglist
//{
//	unsigned int msgpsf; unsigned int msgpscn; struct msglist *next; unsigned char message[];
//}msgnode;
//
//msgnode link1, currlink;

typedef struct framemsg
{
	unsigned char psf; unsigned char pscn; unsigned char msg[42];
}msg_node;

unsigned int pscn_return;

unsigned char vhfmsg[256];

msg_node windowmsg[WINDOW_LENGTH];

unsigned char vhfmsg[256];

void increment_pscn();

void set_pscn(uint32_t val);

uint32_t return_pscn();


void fill_packet(unsigned char *packet, int length);
//void increment_pscn();
unsigned int do_crc32(unsigned char *msg, unsigned int length);
void do_hmac_hash(unsigned char *msg, unsigned char *hashout, unsigned int len);
void do_hmac_encrypt(unsigned char *msg, unsigned char *hmac, unsigned char *key, unsigned int len);
unsigned int frame_decon(unsigned char *frame, unsigned int len, int crypt, deconError_E error);

uint32_t returnPSCN(void);

unsigned int frame_con(unsigned char *packet, unsigned char *frame, unsigned int len, int crypt, unsigned char fvnin, unsigned char aidin, unsigned psfin);
void initKey(void);
//unsigned int dll_process(const char* src, const char* dest, int op/*, unsigned char psf, unsigned char aid, unsigned char fvn*/, unsigned int length, int crypt);  //commented out inputs are to be added later




#endif /* FRAMES_H_ */

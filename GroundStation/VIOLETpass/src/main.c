#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>

#include "getPassInfo.h"
#include "shellcmd.h"
#include "procs.h"
#include "gs_transmission.h"
#include "gs_reception.h"
#include "tcp.h"
#include "queue.h"
#include "file_util.h"
#include "stateMachines.h"
#include "frames.h"

pid_t rxpid;
pid_t txpid;
pid_t dw_pid;
static processId_t processId;  
int direwolf_clientfd;

void sigint_handler(int sig_num){
    printf("\r\nProcess Ended\r\n");
    if (EN_SBAND) {
        zeroMQ_Destroy();
        gnuradio_stoprx(processId.sbandrxPID);
        zeroMQ_PubDestroy();
        gnuradio_stoptx(processId.sbandrxPID);

    }
    
    if (EN_UHFVHF) {
        close(direwolf_clientfd);
        stopDirewolf(dw_pid);
        usleep(500000);
        stop_rigctld();
    }
    exit(0);
}

int main(){
    signal(SIGINT|SIGSEGV, sigint_handler);
//         struct timeval start, stop;
// 
//     
//     
//         gettimeofday(&start, NULL);
// 
//         sleep(2);
//         
//         gettimeofday(&stop, NULL);
//         
//         long timebetweensec = stop.tv_sec - start.tv_sec;
//         double timebetweenusec = (double)(stop.tv_usec - start.tv_usec);
//         double bitrate = (double)( (42*80)/( timebetweensec + (timebetweenusec/1000000)) );
//         printf("bitrate = %f, %ld %f", bitrate, timebetweensec, timebetweenusec);
//     
//     return(0);
    int fd;
    int nBytesSent;
    int nBytesRead;
    int sock;    
    char msg[] = "VE9UNB Testing Transmission from Direwolf";
    char rec[256];

    printf("here");

    tx_freq_E txFreq;
    uint32_t i = 0;
    
//     initFileManagement(RAMDISK_WINDOW_FOLDER_DEST);
    initKey();
    
    /*Check for uplink commands/Fill uplink queue*/    
    string_queue_t uplinkCmdQ;
    queueInitString(&uplinkCmdQ, MAX_STRQ_SIZE);
    
    checkUserUplinkCommands(&uplinkCmdQ, UPLINK_CMDS_DEST);
    
    /*Get Gpredict pass data*/
    uint32_t numDataPoints = readGpredictFile();
    if(!numDataPoints){return(0);}
    
    g_data_t gpredictPassInfo[numDataPoints];
    organizeGpredictData(gpredictPassInfo, numDataPoints);
    createGpredictTestData(gpredictPassInfo, numDataPoints);
    
    /*Determine end of pass*/
    gtime_t passEnd = gpredictPassInfo[numDataPoints - 1].gtime;
    /*Check for Operational Frequencies*/
    avail_freq_E availibleFreq = getAvailibleFreqs();
    

    if (EN_UHFVHF) {
        start_rigctld();
        
        usleep(500000);
        
        dw_pid = forkProc();
        if(dw_pid == -1) {
            printf("ERROR: Fork");
            exit (0);
        }

        if(dw_pid == 0) {
            startDirewolf();
        }

        sleep(1);

        sock = 8001;

        direwolf_clientfd = createClient("127.0.0.1", sock);
             
    }

    /*Start Pass Algorithm*/
        while((i < numDataPoints) && !greaterThanSysTime(&passEnd)){
        
        switch(availibleFreq){
            case BOTH_AVAIL:
                /*Transmission*/
//                 txFreq = gs_TX(gpredictPassInfo[i].El);
                
                break;
            case UHFVHF_AVAIL:
                manageUHFVHF(direwolf_clientfd, &uplinkCmdQ);
                break;
            case SBAND_AVAIL:
                manageSBand(gpredictPassInfo[i].El, processId, &uplinkCmdQ);
                break;
        }//endswitch
    
    /*Time Keeping */
            if(greaterThanSysTime(&(gpredictPassInfo[i].gtime))){
                i++;

                if(DEBUG_GSTX){
                    char * txModeStr;
                    switch(txFreq){
                        case(NONE): txModeStr = "NONE"; break;
                        case(UHFVHF): txModeStr = "UHFVHF"; break;
                        case(SBAND): txModeStr = "SBAND"; break;
                        }
                    //printSysTime();
                    //printf("index : %d of %d\t", i, numDataPoints);
                    //printf("TX MODE: %s\n\r", txModeStr);
                }
        } 
    }//end while
    
    if (EN_SBAND) {
        zeroMQ_Destroy();
        gnuradio_stoprx(processId.sbandrxPID);
    }
    
    if (EN_UHFVHF) {
        close(direwolf_clientfd);
        stopDirewolf(dw_pid);
        usleep(500000);
        stop_rigctld();
    }
    
//     writeFolderNum();
    
    return(0);
}

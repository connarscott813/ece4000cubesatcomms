#include "file_util.h"

#define PATH_MAX 4096
#define BUF_SIZE 256

#define FOLDER_NUM_PATH "/bin/foldernum.bin"


static char * curdir;
static uint32_t folderNumVar;

void initCurDir(void){
     curdir = getcwd(curdir, PATH_MAX);
}

uint32_t getFolderNum(void){
    initCurDir();
    int folderNum = 0;
    char buffer[BUF_SIZE];
    char * path = strcat(curdir, FOLDER_NUM_PATH);
    
    
    FILE * folderNumFile = fopen(path, "rb");
    if(folderNumFile == NULL){ //file doesnt exist
        
        folderNumFile = fopen(path, "wb");
        fprintf(folderNumFile, "0");
        
    }
    else{
        
        fread(buffer, BUF_SIZE, 1, folderNumFile);
        folderNum = atoi(buffer);
    }
    
    fclose(folderNumFile);
    
    folderNumVar = folderNum;
    return(folderNum);

    
}

void writeFolderNum(void){
    initCurDir();
    char * path = strcat(curdir, FOLDER_NUM_PATH);
    
        FILE * folderNumFile = fopen(path, "wb");
        if(folderNumFile == NULL){
            printf("Error opening file\r\n");
            return;
        }
        char integerBuffer[64];
        sprintf(integerBuffer, "%d", folderNumVar);
        if(DEBUG_FILE_UTIL)printf("foldernumfile = %s\r\n", integerBuffer);
        fprintf(folderNumFile, integerBuffer);
        
        fclose(folderNumFile);
    
}

void moveFolder(char * srcFolderDir, char * destFolderDir){
        
        char * cmd[] = {"mv", srcFolderDir, destFolderDir, NULL};
        executeCmd(cmd, 5);
        
}

void createFolder(char * folderName, char * folderPath){
    char buffer[BUF_SIZE];
    strcpy(buffer, folderPath);
    strcat(buffer, "/");
    
    strcat(buffer, folderName);
    
    
    mkdir(buffer, 0777); //0777 give all file permissions
    if(DEBUG_FILE_UTIL)printf("folder created: %s\r\n", buffer);

    
}

/**
 * getPacket.h
 * desc: functions to read, write, and measure bin files. primarily for frame operations.
 * author: Brett Robertson
 * date: Feb, 2022
 *
 */

void storeData(unsigned char PacketData[], uint32_t datasize, const char* PATH){
//    char buffer[BUFFER_SIZE];
    FILE * file;
//    const uint8_t headersize = 0;
    
    int index = 0;
//    uint32_t i=0;

    file = fopen(PATH, "rb");

    if(file == NULL){
    	printf("read error \n");
    	fclose(file);
    	return;
    }

       	  index = fread(PacketData, 1, datasize, file);  //1 for 1 byte at a time
          if (index == -1){
       	  printf("reads \n");
       	  }

    fclose(file);
return;
}

void writeData(unsigned char PacketData[], uint32_t datasize, char* PATH){

	FILE * file;
	file = fopen(PATH, "wb");
    if(file == NULL){
    	printf("write error file NULL \n\r");
    	fclose(file);
    	return;
    }
    fwrite(PacketData, 1, datasize, file);
    if(file == NULL){
    	printf("write error \n\r");
    	fclose(file);
    	return;
    }
    fclose(file);
    return;
}

void appendData(unsigned char PacketData[], uint32_t bigness, const char* PATH){
	FILE * file;
		file = fopen(PATH, "wa");
	    if(file == NULL){
	    	printf("append to bin error \n");
	    	fclose(file);
	    	return;
	    }
	    fwrite(PacketData, 1, bigness, file);
	    if(file == NULL){
	    	printf("append to bin error \n");
	    	fclose(file);
	    	return;
	    }
	    fclose(file);
	    return;
}

uint32_t sizeData(const char* PATH){

		FILE * file;

	   file = fopen(PATH, "rb");

	   uint32_t testlen;

	   fseek(file,0,SEEK_END);
	   testlen = ftell(file);
	   rewind(file);

	   fclose(file);

	   return(testlen);
}

void initFileManagement(char * srcFolderDir){
    char foldBuffer[BUF_SIZE];
    char srcBuffer[BUF_SIZE];
    char integerBuffer[64];
    /*Get folder number from disk*/
    initCurDir();
    getFolderNum();
    
    /*Check to see that current folder num exists/create it*/
    sprintf(integerBuffer, "%d", folderNumVar);
    strcat(integerBuffer, "_window");
    strcpy(foldBuffer, srcFolderDir);
    strcat(foldBuffer, "/");
    strcat(foldBuffer, integerBuffer);
    
    DIR* dir = opendir(foldBuffer);

    if(dir){
        closedir(dir);
    }
    else{
        createFolder(foldBuffer, srcBuffer);
    }
    printf("Created Folder: %s\r\n", foldBuffer);
    
    
}


void writePSCN2File(const char * path, unsigned int pscn, unsigned char msg[], uint32_t msgSize){
    char pathBuffer[BUF_SIZE];
    char integerBuffer[64];
    /*Get window folder name path*/
    initCurDir();
    //folderNumVar = getFolderNum(); //CHANGE THIS TO JUST USE THE VAR
    sprintf(integerBuffer, "/%d_window/", folderNumVar);
    
    strcpy(pathBuffer, path);
    strcat(pathBuffer, integerBuffer);
    
    /*write PSCN to file with pscn num*/
    sprintf(integerBuffer, "%d_pcsn.bin", pscn);
    strcat(pathBuffer, integerBuffer);
    
    printf("filepath: %s\r\n", pathBuffer);
    writeData(msg, msgSize, pathBuffer);

    
}


void moveNcreateWindowFolder(char * srcFolderDir, char * destFolderDir){
    char srcBuffer[BUF_SIZE];
    char moveBuffer[BUF_SIZE];
    char destBuffer[BUF_SIZE];
    char integerBuffer[64];

    /*Initialization of var*/
    initCurDir();

    /*Set paths*/
    strcpy(srcBuffer, srcFolderDir);
    strcpy(destBuffer, destFolderDir);
        //get current folder to move it to dest
    sprintf(integerBuffer, "%d", folderNumVar);
    strcat(integerBuffer, "_window");
    strcpy(moveBuffer, srcFolderDir);
    strcat(moveBuffer, "/");
    strcat(moveBuffer, integerBuffer);
    
    /*Move folder to destination but check to see if it exists first*/
    if(DEBUG_FILE_UTIL) printf("MOVING: %s TO: %s\r\n", moveBuffer,destBuffer);
       
    DIR* dir = opendir(moveBuffer);

    if(dir){
        closedir(dir);
        folderNumVar++;
        moveFolder(moveBuffer, destBuffer);
    }
    
    /*Create new folder to take its place in source*/
    
    /*Make folder folderName*/
    char buffer[PATH_MAX];
    sprintf(integerBuffer, "%d", folderNumVar);
    strcat(integerBuffer, "_window");
    
    strcpy(buffer, srcBuffer);
   
    createFolder(integerBuffer, buffer);
    
}

void checkUserUplinkCommands(string_queue_t * uplinkCmdQ, char * uplinkFilePath){
    char path[BUF_SIZE];
    char buffer[BUF_SIZE];
//     initCurDir();
//     strcpy(path, curdir);
    strcpy(path, uplinkFilePath);
    
    FILE* uplinkFile = fopen(path, "rb");
    
    if(uplinkFile == NULL){
        printf("Error file does not exist\r\n");
        return;
    }
    
    while(fgets(buffer, BUF_SIZE, uplinkFile) != NULL){
        buffer[strcspn(buffer, "\r\n")] = 0;
        enqueueString(uplinkCmdQ, buffer);
    }
    
    fclose(uplinkFile);
    
}




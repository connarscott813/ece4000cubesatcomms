/**
 * @file procs.h
 * @author Connar Scott
 * @brief functions to integrate GNU radio executables/python scripts
 *        and to start Hamlib and direwolf processes
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "shellcmd.h"
#include "config.h"

#ifndef _PROCS_H_
#define _PROCS_H_


/**
 * Starts the gnuradio process
 * @return None
 */
void gnuradio_startrx(void);

/**
 * Stops the gnuradio process
 * @param rx_pid - process ID of the gnuradio rx process
 * @return None
 */
void gnuradio_stoprx(pid_t rx_pid);

/**
 * Starts the gnuradio tx process
 * @param frameSize - size of frame for gnuradio to transmit
 * @return None
 */
void gnuradio_starttx(uint32_t frameSize);

/**
 * Stops the gnuradio tx process
 * @param tx_pid - process ID of gnuradio tx process
 * @return None
 */
void gnuradio_stoptx(pid_t tx_pid);

/**
 * Starts direwolf process
 * @return None
 */
void startDirewolf(void);

/**
 * Stops the direwolf process
 * @param dw_pid - direwolf process id
 * @return None
 */
void stopDirewolf(pid_t dw_pid);

/**
 * Starts the rigctld process and returns the PID
 * @return rigctld pid
 */
int start_rigctld(void);

/**
 * Stops the rigctld process
 * @return None
 */
void stop_rigctld(void);
#endif

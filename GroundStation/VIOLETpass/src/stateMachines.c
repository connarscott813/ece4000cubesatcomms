#include "stateMachines.h"

queue_t errorQ ={
    .capacity = ERROR_Q_SIZE,
    .front = 0,
    .size = 0,
    .rear = ERROR_Q_SIZE - 1  
};

static uint32_t frame_count = 0;
timer_t timer;

/*manageSBand Variables*/
static linkMode_E sbandMode = TX;
static txrxState_E txrxState_s = GET_WINDOW;
static bool sbandRxStart = 1;
static bool sbandRxStop = 0;
//extern static unsigned int pscn;
static bool sbandTxStart = 1;

struct timeval start, stop;
long timebetweensec;
long timebetweenusec;
double bitrate;

void manageSBand(float elevation, processId_t  processId, string_queue_t * uplinkCmdQ){
    unsigned int len;
    uint32_t errorPSCN = 0;
    unsigned char msg[MAX_PACKET_LENGTH];
    
    unsigned char recievedFrame[MAX_PACKET_LENGTH + DLL_OVERHEAD];
    
    
    switch(sbandMode){
    case TX:
        switch(txrxState_s){
            case GET_WINDOW:
                printf("1:TX GET_WINDOW\r\n");
                if(sbandTxStart){
                    /*Check for uplink cmds*/
                    if(isStringQEmpty(uplinkCmdQ)){
                        strcpy((char*)msg, DEFAULT_UPLINK_CMD);
                        len = strlen((char*)msg);
                    }
                    else{
                        dequeueString(uplinkCmdQ, (char*)msg);
                        len = strlen((char*)msg);
                    }
                    printf("Uplink: %s\r\n", msg);
                    /*Start GNU radio proc + zeroMQ*/
                    processId.sbandtxPID = forkProc();
                    if(processId.sbandrxPID == -1){ 
                            perror("Error:Fork\r\n"); 
                            exit(1);
                        }
                    sbandTxStart = 0;
                    if(processId.sbandtxPID == CHILD){
                        gnuradio_starttx(len+DLL_OVERHEAD);
                    }
                    zeroMQ_PubInit();
                    sleep(GNURADIO_STARTUP_TIME+2);
                }
                
                unsigned char tempframe[MAX_PACKET_LENGTH+DLL_OVERHEAD];
                frame_con(msg, tempframe, len, 1, FVN_DEFAULT, AID_DEFAULT, PSF_DEFAULT);
  
                zeroMQ_PubWrite((unsigned char *)tempframe, len+DLL_OVERHEAD);
                usleep(ZEROMQ_WRITE_TIME);
                zeroMQ_PubDestroy();
                               
                /*Set Next States*/
                txrxState_s = GET_WINDOW;
                sbandMode = RX;
                sbandTxStart = 1;
                gnuradio_stoptx(processId.sbandtxPID);
                break;
            
            case GET_ERRORS:
                printf("3:TX GET_ERRORS\r\n");
                //enqueue(&errorQ,0x48656C6C);
                
                /*Set up PSCN array*/
                unsigned char pscnArray[MAX_PACKET_LENGTH];
                unsigned char frameDest[MAX_PACKET_LENGTH + DLL_OVERHEAD]; 
                
                if(isQEmpty(&errorQ)){
                    strcpy((char*)pscnArray, NO_ERROR_UPLINK);
                    len = strlen((char*)pscnArray);
                } 
                else{
                    len = errorQ.size*4;
                    queue2array(&errorQ, pscnArray, len);

                    printQueue(&errorQ);
                }
                
                /*Transmit*/
                if(sbandTxStart){
                 processId.sbandtxPID = forkProc();
                 if(processId.sbandtxPID == -1){ 
                        perror("Error:Fork\r\n"); 
                        exit(1);
                    }
                 sbandTxStart = 0;                
                 
                 printf("len=%d\r\n", len + DLL_OVERHEAD);
                 if(processId.sbandtxPID == CHILD){
                     gnuradio_starttx(len + DLL_OVERHEAD);
                 }
                 zeroMQ_PubInit();
                 sleep(GNURADIO_STARTUP_TIME+2);
                }
                
             
                printf("pscnArray:");
                printArray(pscnArray,len);
                printf("\r\n");
                
                
                frame_con(pscnArray, frameDest, len, 1, FVN_DEFAULT, AID_DEFAULT, PSF_DEFAULT);

                printf("frameDest: ");
                printArray(frameDest,len+DLL_OVERHEAD);
                printf("\r\n");
                                
                zeroMQ_PubWrite((unsigned char *)frameDest, len + DLL_OVERHEAD+1);
                usleep(ZEROMQ_WRITE_TIME);
                zeroMQ_PubDestroy();
                
                /*Set Next States*/
                if(isQEmpty(&errorQ)){
                    txrxState_s = GET_WINDOW; 
                    sbandMode = TX;
                    sbandTxStart = 1;
                    gnuradio_stoptx(processId.sbandtxPID);
                }
                else{
                    txrxState_s = GET_ERRORS; 
                    sbandMode = RX;
                    sbandTxStart = 1;
                    gnuradio_stoptx(processId.sbandtxPID);
                }
                sleep(GNURADIO_STOP_TIME);
                break;
        }
        break;
    case RX:
        switch(txrxState_s){
            case GET_WINDOW:
                if(sbandRxStart){
                    printf("2:RX GET_WINDOW\r\n");
                    processId.sbandrxPID = forkProc();
                    if(processId.sbandrxPID == -1){ 
                        perror("Error:Fork\r\n"); 
                        exit(1);
                    }
                    sbandRxStart = 0;
                    if(processId.sbandrxPID == CHILD) gnuradio_startrx();
                    sleep(GNURADIO_STARTUP_TIME);
                    zeroMQ_ReqInit();
                }
                /*Check for recieved frame*/
                
                if(zeroMQ_ReqRead(recievedFrame, &len)){
                    int i;
                    if(frame_count == 0){
                       gettimeofday(&start, NULL);
                    }
                    printf("len for decon is %d\r\n", len);
                    
                    /*Frame Deconstruction + adding errors to queue*/
                    
                    errorPSCN = frame_decon(recievedFrame, len , 0, NOT_ERROR_FRAME);
                        
                    if(errorPSCN){
                        enqueue(&errorQ, errorPSCN);
                    }
                    else{
//                         writePSCN2File(RAMDISK_WINDOW_FOLDER_DEST, pscn, windowmsg[frame_count].msg, len-DLL_OVERHEAD);
                        printf("windowmsg is: ");
                        for (i=0;i<10;i++){
                         printf("%c", windowmsg[frame_count].msg[i]);   
                        }
                        printf("\n");
                    }
                    
                    frame_count++;
                                    
                    if(frame_count >= 10 || sbandRxStop){
                        
                        gettimeofday(&stop, NULL);
                    
                        long timebetweensec = stop.tv_sec - start.tv_sec;
                        long timebetweenusec = (stop.tv_usec - start.tv_usec);
                        double bitrate = (double)( (8*frame_count*42)/( timebetweensec + (timebetweenusec/1000000) - 1.80) );
                        printf("bitrate = %f", bitrate);
                        
                        frame_count = 0;
                        zeroMQ_Destroy();
                        /*Set Next States*/
                        sbandRxStart=1;

                        gnuradio_stoprx(processId.sbandrxPID);
                        txrxState_s = GET_ERRORS;
                        sbandMode = TX;
                        sleep(GNURADIO_STOP_TIME);
                    }
                }
            break;
            case GET_ERRORS:
                
                    if(sbandRxStart){
                        printf("4:RX GET_ERRORS\r\n");
                        printQueue(&errorQ);
                        processId.sbandrxPID = forkProc();
                        if(processId.sbandrxPID == -1){ 
                            perror("Error:Fork\r\n"); 
                            exit(1);
                        }
                        sbandRxStart = 0;
                        if(processId.sbandrxPID == CHILD) gnuradio_startrx();
                        sleep(GNURADIO_STARTUP_TIME);
                        zeroMQ_ReqInit();
                    }
                
                    if(zeroMQ_ReqRead(recievedFrame, &len)){
                        printf("Enter Receiving Error");
                        errorPSCN = frame_decon(recievedFrame, len, 0, ERROR_FRAME);
                        
                        if(!errorPSCN){
//                            writePSCN2File(RAMDISK_WINDOW_FOLDER_DEST, returnPSCN(), windowmsg[frame_count].msg, len-DLL_OVERHEAD);
                            queueRemove(&errorQ, returnPSCN());
   
                        }
                    
                    
                    /*Set Next States*/
                    if(isQEmpty(&errorQ)){
                        txrxState_s = GET_WINDOW;
                        sbandMode = TX;
                        zeroMQ_Destroy();
                        sbandRxStart=1;
                        gnuradio_stoprx(processId.sbandrxPID);
//                         moveNcreateWindowFolder(RAMDISK_WINDOW_FOLDER_DEST, 
//                                                DISK_WINDOW_FOLDER_DEST);
                    }
                    else{
                        txrxState_s = GET_ERRORS;
                        sbandMode = TX;  
                        zeroMQ_Destroy();
                        sbandRxStart=1;
                        gnuradio_stoprx(processId.sbandrxPID);
                    }
                    sleep(GNURADIO_STOP_TIME);
                }
           
            break;
        }
    }
}

/*manageUHFVHF Variables*/
static linkMode_E UHFVHFMode = TX;
static txrxState_E txrxState_uv = GET_WINDOW;

void manageUHFVHF(int direwolf_fd, string_queue_t * uplinkCmdQ){
    bool flag;
    int bytesRecv;
    char rec[256];
    char msg[256];
    int len;
    
    flag = true;
     
    switch(UHFVHFMode){
    case TX:
        //usleep(50000);
        switch(txrxState_uv){
            case GET_WINDOW:
                
                if (flag) {
                    /*Check for uplink cmds*/
                    if(isStringQEmpty(uplinkCmdQ)){
                        strcpy((char*)msg, DEFAULT_UPLINK_CMD);
                        len = strlen((char*)msg);
                    }
                    else{
                        dequeueString(uplinkCmdQ, (char*)msg);
                        len = strlen((char*)msg);
                    }
                    
                    transmitVHFUHF(direwolf_fd, msg);
                    flag = false;
                }
                txrxState_uv = GET_WINDOW;
                UHFVHFMode = RX;
                break;
            
            case GET_ERRORS:
 //               transmitVHFUHF(direwolf_fd, list of errors);
                txrxState_uv = GET_ERRORS;
                UHFVHFMode = RX;
                break;
        }
        break;
    case RX:
        usleep(50000);
        switch(txrxState_uv){
            case GET_WINDOW:
                bytesRecv = receiveVHFUHF(direwolf_fd, rec);
                if(bytesRecv != -1) {
                    flag = true; 
                    UHFVHFMode = TX;
                    txrxState_uv = GET_WINDOW;
                }
            break;
            case GET_ERRORS:
        
                /*Set Next States*/
                    if(isQEmpty(&errorQ)){
                        txrxState_uv = GET_WINDOW;
                        UHFVHFMode = TX;
                    }
                    else{
                        txrxState_uv = GET_ERRORS;
                        UHFVHFMode = TX;                
                    }
            break;
        }
    }
}

tx_freq_E returnTxMethod(float elevation){
    tx_freq_E method;
    
    if((elevation >= MIN_AOS_SBAND) && EN_SBAND){
        method = SBAND;
    }
    else if((elevation >= MIN_AOS_UHFVHF) && EN_UHFVHF){
        method = UHFVHF;
    }
    else{
        method = NONE;
    }
    
    return(method);
}

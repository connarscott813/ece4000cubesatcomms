#include "gs_transmission.h"
#include "frames.h"

   

avail_freq_E getAvailibleFreqs(void){
    if(EN_SBAND && EN_UHFVHF) return(BOTH_AVAIL);
    else if(EN_UHFVHF)        return(UHFVHF_AVAIL);
    else                      return(SBAND_AVAIL);
}

static zsock_t * pub;

void zeroMQ_PubInit(void){
    pub = zsock_new_pub(PUB_SERVER);
    assert(pub);
    printf("pub asserted\r\n");
}

void zeroMQ_PubWrite(unsigned char * msg, int len){
    //strcat(msg, "");
    /*int ret = */zsock_send(pub, "b" ,msg, len);
    //printf("returnValue = %d\r\n",ret);
}

void zeroMQ_PubDestroy(void){
 
    zsock_destroy(&pub);
    
}

void createGpredictTestData(g_data_t * gdataStruct, uint32_t numDataPoints){
 
    uint32_t i;
    
    getSysTime();
    setGtimeToSysTime(&gdataStruct[0].gtime);
    printf("%s %s %s\r\n", gdataStruct[0].gtime.hour, gdataStruct[0].gtime.minute,gdataStruct[0].gtime.second);

    for(i = 1; i < numDataPoints; i++){
       changeGtime(&gdataStruct[i].gtime,&gdataStruct[i-1].gtime);
    }
   if(DEBUG_GSTX){ printgdata(gdataStruct, numDataPoints); }
}

void transmitVHFUHF(int fd, char *message) {
    uint8_t FEND = 0xC0;
    uint8_t FESC = 0xDB;
    uint8_t TFEND = 0xDC;
    uint8_t TFESC = 0xDD;
    
    
    uint8_t DATA_cmd = 0x00;
    uint8_t DELAY_cmd = 0x01;
    int nBytesSent;
    int longness;
    
    write(fd,&FEND,1);
    write(fd,&DATA_cmd,1); //Send data frame command to Direwolf
    longness = strlen(message);
    char frame[longness+18];
    frame_con(message, frame, longness, 1, FVN_DEFAULT, AID_DEFAULT, PSF_DEFAULT);
    
    nBytesSent = write(fd,frame,longness+18);
    
    printf("Bytes Sent: %d \n", nBytesSent); 
    write(fd,&FEND,1);
    printf("\n");
    
    sleep(2);
    
    return;
}

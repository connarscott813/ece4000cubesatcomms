/**
 * @file shellcmd.h
 * @author Connar Scott
 * @brief Functions for executing shell commands and creating
 *        basic pipes
 */

#ifndef _SHELLCMD_H_
#define _SHELLCMD_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>

#define DEBUG_SHELLCMD 0

#define CHILD 0

typedef enum pipeCmd{
  READ,
  WRITE
}pipeCmd_E;

typedef int pipefd_t[2];

/***************PUBLIC FUNCTIONS******************/

/**
 * Appends parameters to atd syntax from aos_to_atsyntax.
 * @param cmd - array of strings containing command, last element must be
 *              NULL;
 * @param cmdSize - size of command array
 * @return None
 */
void executeCmd(char ** cmd, uint32_t cmdSize);

/**
 * Wrapper for forking a new process
 * @return process ID of new process
 */
pid_t forkProc(void);

/**
 * Wrapper for waiting on child process
 * @param pid - process ID of child that is being waited on
 * @return none
 */
void waitProc(pid_t pid);

/***************PRIVATE FUNCTIONS******************/

/**
 * Executes a cmd
 * @param cmd - array of strings containing command, last element must be
 *              NULL
 * @param cmdSize - size of command array
 * @return none
 */
void execute(char **cmd, uint32_t cmdSize);

/**
 * Checks for a '>' delim in a command to alter stdout
 * to desired location
 * @param cmd - array of strings containing command, last element must be
 *              NULL
 * @param cmdSize - size of command array
 * @return none
 */
char * checkRedirect(char **cmd, uint32_t cmdSize);

/**
 * Executes a cmd with  redirected STDOUT
 * @param cmd - array of strings containing command, last element must be
 *              NULL
 * @param cmdSize - size of command array
 * @param redirectLoc - location of the redirected STDOUT
 * @return none
 */
void executeRedirect(char **cmd, uint32_t cmdSize, char *redirectLoc);

/**
 * Creates a pipe using the pipefd file descriptor
 * @param pipefd - pipe file descriptor
 * @return none
 */
void createPipe(pipefd_t pipefd);

/**
 * Closes the indicated pipe end
 * @param pipeOp - pipeCmd enum either WRITE or READ
 * @param pipefd - pipe file descriptor
 * @return none
 */
void closePipeEnd(pipeCmd_E pipeOp, pipefd_t pipefd);

/**
 * Performs indicated pipe operation
 * @param pipeOp - pipeCmd enum either WRITE or READ
 * @param pipefd - pipe file descriptor
 * @param argv[] - argument values
 * @param argc - size of argv
 * @return none
 */
int pipeOperation(pipeCmd_E pipeOp, pipefd_t pipefd, char argv[],  int argc);

#endif

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Tx GNU Radio (LimeSDR)
# Author: Thomas Boyle
# Description: Flowgraph to transmit a file from the LimeSDR over S-band
# Generated: Sun Mar 13 18:46:36 2022
##################################################


from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import zeromq
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from grc_gnuradio import blks2 as grc_blks2
from optparse import OptionParser
import limesdr
import sys

class tx_gnuradio(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Tx GNU Radio (LimeSDR)")

        ##################################################
        # Variables
        ##################################################
        self.sps = sps = 2
        self.sdr_gain = sdr_gain = 50
        self.samp_rate = samp_rate = 4e6
        self.centre_freq = centre_freq = 2.4054e9

        ##################################################
        # Blocks
        ##################################################
        self.zeromq_sub_source_0 = zeromq.sub_source(gr.sizeof_char, 1, 'tcp://127.0.0.1:5557', 100, False, -1)
        self.limesdr_sink_0 = limesdr.sink('0009083401881019', 0, '', '')
        self.limesdr_sink_0.set_sample_rate(samp_rate)
        self.limesdr_sink_0.set_center_freq(centre_freq, 0)
        self.limesdr_sink_0.set_bandwidth(5e6,0)
        self.limesdr_sink_0.set_gain(sdr_gain,0)
        self.limesdr_sink_0.set_antenna(1,0)
        self.limesdr_sink_0.calibrate(5e6, 0)

        self.digital_gmsk_mod_0 = digital.gmsk_mod(
        	samples_per_symbol=sps,
        	bt=0.25,
        	verbose=False,
        	log=False,
        )
        self.blks2_packet_encoder_0 = grc_blks2.packet_mod_b(grc_blks2.packet_encoder(
        		samples_per_symbol=sps,
        		bits_per_symbol=1,
        		preamble='',
        		access_code='',
        		pad_for_usrp=True,
        	),
        	payload_length=int(sys.argv[1]),
        )



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blks2_packet_encoder_0, 0), (self.digital_gmsk_mod_0, 0))
        self.connect((self.digital_gmsk_mod_0, 0), (self.limesdr_sink_0, 0))
        self.connect((self.zeromq_sub_source_0, 0), (self.blks2_packet_encoder_0, 0))

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps

    def get_sdr_gain(self):
        return self.sdr_gain

    def set_sdr_gain(self, sdr_gain):
        self.sdr_gain = sdr_gain
        self.limesdr_sink_0.set_gain(self.sdr_gain,0)
        self.limesdr_sink_0.set_gain(self.sdr_gain,1)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_centre_freq(self):
        return self.centre_freq

    def set_centre_freq(self, centre_freq):
        self.centre_freq = centre_freq
        self.limesdr_sink_0.set_center_freq(self.centre_freq, 0)


def main(top_block_cls=tx_gnuradio, options=None):

    tb = top_block_cls()
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()

#ifndef _CONFIG_H_
#define _CONFIG_H_

/*File Paths*/
#define GPREDICT_DATA_PATH (const char*) "/data/gpredict_next_pass_test.txt"

#define GNURADIO_RX "/home/eceuser/Desktop/ece4000cubesatcomms/SpacePC/bin/rx_sp_violet.py"
#define GNURADIO_TX "/home/eceuser/Desktop/ece4000cubesatcomms/SpacePC/bin/tx_sp_violet.py"

/*Project Parameters */
#define EN_SBAND 0
#define EN_UHFVHF 1

#define MIN_AOS_UHFVHF (float)10.0f
#define MIN_AOS_SBAND (float)25.0f

#define FRAME_SIZE 160 //Bytes

#define ERROR_Q_SIZE 256

#define DEFAULT_UPLINK_CMD "Give me packets or else"
#define NO_ERROR_UPLINK "00000"
/*General Parameters*/

#define SEC_TO_NSEC(seconds) (uint32_t)(1000000000*seconds) 

#define FVN_DEFAULT 0x00
#define AID_DEFAULT 0x00
#define PSF_DEFAULT 0x03

#endif

/**
 * @file getPassInfo.h
 * @author Connar Scott
 * @brief Functions for processsing pass information from 
 *        pass information txt file produced by Gpredict
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"

#ifndef _GETPASSINFO_H_
#define _GETPASSINFO_H_

#define BUFFER_SIZE 128
#define DEBUG_GPI 0

#define HEADER_SIZE 3

typedef struct gdate{
    char year[3];
    char month[3];
    char day[3];
}gdate_t;

typedef struct gtime{
    char hour[3];
    char minute[3];
    char second[3];
}gtime_t;

typedef struct g_data{
    gdate_t gdate;
    gtime_t gtime;
    float Az;
    float El;
}g_data_t;

/**
 * Reads in gpredict txt file and returns number of lines - header
 * @return number of lines minus header size
 */
uint32_t    readGpredictFile(void);

/**
 * Stores/organizes gpredict pass information into the gpredict structure
 * @param gpredictStruct[] - gpredict_data struct array
 * @param datasize - number of lines in txt file
 * @return None
 */
void        organizeGpredictData(g_data_t gpredictStruct[], uint32_t datasize);

/**
 * Processes and stores pass information to gpredict structure
 * @param gpredictStruct[] - gpredict_data struct array
 * @param datasize - number of lines in txt file
 * @param file - pointer to a txt file
 * @return None
 */
void        storeGpredictData(g_data_t gpredictData[], uint32_t datasize, FILE * file);

/**
 * Sets date parameters in gpredict structure
 * @param gpredictStruct[] - gpredict_data struct array
 * @param datasize - number of lines in txt file
 * @param file - pointer to a txt file
 * @param token - pointer to token with date information
 * @return None
 */
void        set_gdate(g_data_t gpredictData[], uint32_t datasize, uint32_t index, char *token);

/**
 * Sets time parameters in gpredict structure
 * @param gpredictStruct[] - gpredict_data struct array
 * @param datasize - number of lines in txt file
 * @param file - pointer to a txt file
 * @param token - pointer to token with time information
 * @return None
 */
void        set_gtime(g_data_t gpredictData[], uint32_t datasize, uint32_t index, char *token);

/**
 * Converts string to tokens and returns token count
 * @param *string - pointer to string to be tokenized
 * @param **tokenArray - pointer to array where tokens are to be stored
 * @param tokenArraySize - size of the token array
 * @param delim - string to seperate values by
 * @return Number of tokens
 */
int         string2tokens(char string[], char* tokenArray[], const int tokenArraySize, const char* delim );

/**
 * Prints contents of gpredict structre - for debugging
 * @param gpredictStruct[] - gpredict_data struct array
 * @param datasize - number of lines in txt file
 * @return None
 */
void        printgdata(g_data_t gpredictData[], uint32_t datasize);

/**
 * Returns the number of lines in a txt file
 * @param file - pointer to a txt file
 * @return number of lines in txt file
 */
uint32_t    getFileSize(FILE * file);


#endif

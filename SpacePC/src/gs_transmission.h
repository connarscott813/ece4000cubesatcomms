/**
 * @file gs_transmission.h
 * @author Connar Scott, Ethan Brewer
 * @brief Ground Station transmission functions
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "timecmpr.h"
#include "shellcmd.h"
#include "getPassInfo.h"
#include "procs.h"
#include "config.h"
#include "queue.h"
#include "aes.h"
#include "file_util.h"
#include "gs_reception.h"

#ifndef _GS_TRANSMISSION_H_
#define _GS_TRANSMISSION_H_

#define DEBUG_GSTX 1
#define PUB_SERVER "tcp://127.0.0.1:5557"

typedef enum tx_freq{
    UHFVHF,
    SBAND,
    NONE
}tx_freq_E;

typedef enum tx_mode{
    PACKET,
    FM_TRANSPONDER,
    PLAYBACK
}tx_mode_E;

typedef enum avail_freq{
    BOTH_AVAIL,
    SBAND_AVAIL,
    UHFVHF_AVAIL
}avail_freq_E;

/**
 * Returns the availible frequencies based on config.h
 * @return availible frequency as a avail_freq enum
 */

avail_freq_E getAvailibleFreqs(void);

/**
 * Alters the gpredict test data for testing purposes,
 * where every data point's time value is changed to be
 * the current time + 1 second/ data point
 * @param gdataStruct - pointer to the g_data_t struct 
 *                      containing gpredict data
 * @param numDataPonts - The size of the gpredict data structure
 * @return None
 */
void createGpredictTestData(g_data_t * gdataStruct, uint32_t numDataPoints);

/**
 * Initializes the PUB server for zeroMQ
 * @return None
 */
void zeroMQ_PubInit(void);

/**
 * Destroys the PUB server for zeroMQ
 * @return None
 */
void zeroMQ_PubDestroy(void);

/**
 * Writes msg to SUB zeroMQ clients
 * @param msg - message to send
 * @param len - length of message
 * @return None
 */
void zeroMQ_PubWrite(unsigned char * msg, int len);

/**
 * Writes message to direwolf server to transmit
 * @param fd - file descriptor for direwolf tcp port
 * @param message - pointer to message to send
 * @return None
 */
void transmitVHFUHF(int fd, char *message);
#endif

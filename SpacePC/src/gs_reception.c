#include "gs_reception.h"

static zsock_t * req;

void zeroMQ_ReqInit(void){
   req = zsock_new_req(REP_SERVER);
   //zsock_set_unbounded(req);
   assert(req);
}

bool zeroMQ_ReqRead(unsigned char * buffer, unsigned int * length){
   size_t len;
   int retries_left = REQ_RETRIES;
  
   while(retries_left){
        char * request = "\x01\x00\x00\x00";
        zstr_send(req, request);
        int expect_reply = 1;
        while(expect_reply){
            //printf("Expecting reply...\r\n");
            zmq_pollitem_t items[] ={{zsock_resolve(req), 0, ZMQ_POLLIN, 0}};
            //printf("After polling..\r\n");
            int rc = zmq_poll(items, 1, REQUEST_TIMEOUT * ZMQ_POLL_MSEC);
            if(rc == -1){
                break; //interrrupted
            }
            /*process reply and exit loop if relpy is valid, or try again and repeat until tries run out*/
            
            if(items[0].revents & ZMQ_POLLIN){
             //printf("Herein if\r\n");
             byte * reply = NULL;
             zsock_recv(req, "b", &reply, &len);
             *length = len;  //set length based on recieved
             //strcat(reply, "");
             if(!reply){
                break;
             }
             else{
                //printf("I: server replied (%s)\r\n", reply);
                printArray(reply, len);
                for (uint32_t i=0;i<len;i++){
                    buffer[i]=reply[i];
                }
                //buffer = (unsigned char *)buffer;
                zstr_free((char**)&reply);
                return(1);
             }
             zstr_free((char**)&reply);
            }
            else{
                if(--retries_left == 0){
                    //printf("Error Server seems to be offline,exit\r\n");
                    break;
                }
                else{
                    //printf("No response retrying\n\r");
              /*      zsock_destroy(&req);
                    printf("Reconnecting to server\r\n");
                    req = zsock_new_req(REP_SERVER);
                    assert(req)*/;
                   //zstr_send(req, request);
                }
                //printf("Retries Left: %d", retries_left);
            }
        }
        
    }
    return(0);

}

void zeroMQ_Destroy(void){
 zsock_destroy(&req);   
}

int getArrayLength(char array[], int maxArraySize){
 
    int count = 0;
    
    while(array[count] != '\0' && count < maxArraySize){
        count++;
    }
    
    return(count);
    
}

void printArray(unsigned char * array, uint32_t size){
    
    for(uint32_t i = 0; i < size; i++){
        printf("%x", array[i]);
    }
    printf("\n\r");
    
}

bool compareArray(unsigned char * array1, unsigned char * array2, uint32_t size){
    int i;
    for(i = 0; i < size; i++){
     if(array1[i] != array2[i]){
        return(0);
     }
    }
    
    return(1);
    
}

int receiveVHFUHF(int fd, char *rec) {
    int nBytesRead;
    char buffer[256] = {0};
   // rec = (char*)malloc(100);
    
    nBytesRead = recv(fd,buffer,256,MSG_DONTWAIT);
    
    for(int i=2; i < nBytesRead-1; i++) {
        rec[i-2] = buffer[i];
        printf("%c",rec[i-2]);
    }
    
    if(nBytesRead != -1) {
        frame_decon_vhf(rec, nBytesRead-3, 1, ERROR_FRAME);
        printf("%d\n", nBytesRead);
    }
    if(nBytesRead == -1) 
        return nBytesRead;
    else 
        return nBytesRead-3;
    
}


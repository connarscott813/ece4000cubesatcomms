#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>

#include "getPassInfo.h"
#include "shellcmd.h"
#include "procs.h"
#include "gs_reception.h"
#include "gs_transmission.h"
#include "tcp.h"
#include "config.h"
#include "frames.h"

#define MAX_PACKET_LENGTH 255

#define GNURADIO_STARTUP_TIME 4
#define GNURADIO_SHUTDOWN_TIME 1
#define ZEROMQ_DELAY 200000

#define SBAND_REQUEST (unsigned char *)"Give me packets or else!"
#define UHFVHF_REQUEST (unsigned char *) "VE9UNB Please sir, can I have some more packetssss"

typedef enum linkMode{
        TX,
        RX
}linkMode_E;

typedef enum txrxState{
    GET_WINDOW,
    GET_ERRORS
}txrxState_E;

pid_t rxpid;
pid_t txpid;
pid_t direwolf_pid;

void sigint_handler(int sig_num){
    printf("\r\nProcess Ended\r\n");
    zeroMQ_Destroy();
    zeroMQ_PubDestroy();
    stopDirewolf(direwolf_pid);
    gnuradio_stoptx(txpid);
    gnuradio_stoprx(rxpid);
    exit(0);
}

int main(){
     signal(SIGINT|SIGSEGV, sigint_handler);
     initKey();

    uint32_t i = 0;

    /*Start Pass Algorithm*/
    if(EN_SBAND && !EN_UHFVHF){
        uint32_t frame_count = 0;
        int len;
        bool txFlag = 1;
        
        tx_freq_E txFreq;
        bool sbandRxStart = 1;
        linkMode_E sbandMode = RX;
        txrxState_E txrxstate_s = GET_WINDOW;
        
        unsigned char recievedFrame[MAX_PACKET_LENGTH+DLL_OVERHEAD];
        unsigned char tempacket[] = "Give me packets or else!";
        unsigned char errorpacket[] = "Resending errors!!"; 
//         unsigned char errorpacket[MAX_PACKET_LENGTH+DLL_OVERHEAD]; 
        unsigned char tempframe[MAX_PACKET_LENGTH+DLL_OVERHEAD];
        
        uint32_t tempPSCN;
        uint32_t num_errors;
        
        timer_t timer;
        oneShotTimerInit(&timer);
        
        while(i < 300){
        
            
        i++;
        switch(sbandMode){
        case TX:
            switch(txrxstate_s){
                case GET_WINDOW:
                  //sleep(1);
                    
                    len = strlen((char*)tempacket);
                    if(txFlag){
                        printf("In TX WINDOW\r\n");
                        txpid = forkProc();
                        if(txpid == -1){ 
                                perror("Error:Fork\r\n"); 
                                exit(1);
                            }
                            txFlag = 0;
                        if(txpid == CHILD){ gnuradio_starttx(len+DLL_OVERHEAD);}
                        sleep(GNURADIO_STARTUP_TIME+2);
                        //oneShotTimer(&timer, SEC_TO_NSEC(3));
                        zeroMQ_PubInit();
                    }

                    frame_con(tempacket, tempframe, len, 0, FVN_DEFAULT, AID_DEFAULT, PSF_DEFAULT);


                    zeroMQ_PubWrite(tempframe, len+DLL_OVERHEAD);
                    usleep(ZEROMQ_DELAY);

                    frame_count++;
                    increment_pscn();

                    /*Set Next States*/
                    if(frame_count > 10){
                        //printOneShotTime(&timer, SEC_TO_NSEC(3), len*10);
                        zeroMQ_PubDestroy();
                        sbandMode = RX;
                        txrxstate_s = GET_ERRORS;
                        gnuradio_stoptx(txpid);
                        sleep(GNURADIO_SHUTDOWN_TIME);
                        frame_count = 0;
                        txFlag = 1;
                    }
                    break;
                case GET_ERRORS:
                    len = strlen((char*)tempacket);
                    if(txFlag){
                        printf("In TX ERRORS\r\n");
                        txpid = forkProc();
                        if(txpid == -1){ 
                                perror("Error:Fork\r\n"); 
                                exit(1);
                            }
                        txFlag = 0;
                        num_errors = 1;
                        //tempPSCN = return_pscn(); //just for testing purposes
                        printf("len=%d\r\n", len+DLL_OVERHEAD);
                        if(txpid == CHILD){gnuradio_starttx(len+DLL_OVERHEAD);}
                        sleep(GNURADIO_STARTUP_TIME+2);
                        zeroMQ_PubInit();

                    }
                    
                    //set_pscn(0x48656C6C);
                    
                    frame_con(tempacket, tempframe, len, 0, FVN_DEFAULT, AID_DEFAULT, PSF_DEFAULT);

                    printf("len=%d\r\n", len+DLL_OVERHEAD);
                    zeroMQ_PubWrite(tempframe, len+DLL_OVERHEAD);
                    usleep(ZEROMQ_DELAY);
                

                    //pscn++;
                    if(1){ //frame_count < num_errors){
                        /*Set Next States*/
                        //set_pscn(tempPSCN);
                        zeroMQ_PubDestroy();
                        sbandMode = RX;
                        txrxstate_s = GET_WINDOW;
                        gnuradio_stoptx(txpid);
                        sleep(GNURADIO_SHUTDOWN_TIME);
                        frame_count = 0;
                        txFlag = 1;
                    }
                    break;
                
            }
            break;
        case RX:
            switch(txrxstate_s){
                case GET_WINDOW:
                if(sbandRxStart){
                    printf("In RX WINDOW\n\n\r");
                    rxpid = forkProc();
                    if(rxpid == -1){ 
                        perror("Error:Fork\r\n"); 
                        exit(1);
                    }
                    sbandRxStart = 0;
                    if(rxpid == CHILD) gnuradio_startrx();
                    sleep(GNURADIO_STARTUP_TIME);
                    zeroMQ_ReqInit();
                }

                if(zeroMQ_ReqRead(recievedFrame, &len)){ //set length based on received
                    
                    printf("len=%d\n\r", len);
                    
                    for (i=0;i<len;i++){
                        printf("%x", recievedFrame[i]);
                    }
                    
                    
                    frame_decon(recievedFrame, len, 1, NOT_ERROR_FRAME);
                    
                    printf("received frame: ", recievedFrame);
                    printArray(recievedFrame, len);
                    printf("RECIEVED FRAME: ");
                    printArray(windowmsg[frame_count].msg, len-DLL_OVERHEAD);
                    
                    increment_pscn();
                    if(compareArray(windowmsg[frame_count].msg, SBAND_REQUEST, len-DLL_OVERHEAD)){
                        printf("REQUEST RECEIVED\n\r");
                        frame_count = 0;
                        zeroMQ_Destroy();
                        /*Set Next States*/
                        gnuradio_stoprx(rxpid);
                        sbandRxStart = 1;
                        txrxstate_s = GET_WINDOW;
                        sbandMode = TX;
                        sleep(GNURADIO_SHUTDOWN_TIME);

                    }
                
                }
                    break;
                case GET_ERRORS:
                    if(sbandRxStart){
                        printf("In RX ERRORS\n\n\r");
                        rxpid = forkProc();
                        if(rxpid == -1){ 
                            perror("Error:Fork\r\n"); 
                            exit(1);
                        }
                        sbandRxStart = 0;
                        if(rxpid == CHILD) gnuradio_startrx();
                        sleep(GNURADIO_STARTUP_TIME);
                        zeroMQ_ReqInit();
                    }


                    if(zeroMQ_ReqRead(recievedFrame, &len)){ //set length based on received
                        
                        printf("len=%d\n\r", len);
                        
                        for (i=0;i<len;i++){
                            printf("%x", recievedFrame[i]);
                        }
                        
                        
                        frame_decon(recievedFrame, len, 1, ERROR_FRAME);
                        
                        printf("received frame: ", recievedFrame);
                        printArray(recievedFrame, len);
                        printf("RECIEVED FRAME: ");
                        printArray(windowmsg[13].msg, len-DLL_OVERHEAD);
                        if(compareArray(windowmsg[13].msg, NO_ERROR_UPLINK, len-DLL_OVERHEAD)){
                            zeroMQ_Destroy();
                            /*Set Next States*/
                            gnuradio_stoprx(rxpid);
                            sbandRxStart = 1;
                            txrxstate_s = GET_WINDOW;
                            sbandMode = RX;
                        }
                        else{
                            zeroMQ_Destroy();
                            /*Set Next States*/
                            gnuradio_stoprx(rxpid);
                            sbandRxStart = 1;
                            txrxstate_s = GET_ERRORS;
                            sbandMode = TX;
                            
                            
                        }
                        sleep(GNURADIO_SHUTDOWN_TIME);
                    }
                    break;
            }
        break;
        
    }
                    
        } //end while
            zeroMQ_Destroy();
            gnuradio_stoprx(rxpid);
            zeroMQ_PubDestroy();
            gnuradio_stoptx(txpid);
    }
    else if(!EN_SBAND && EN_UHFVHF){
            linkMode_E uhfvhfMode = RX;
            bool waitForUplink = 1;
            char msg[256];
            int msgSize;
            
            start_rigctld();
            usleep(500000);
            
            direwolf_pid = forkProc();
            if(direwolf_pid == -1){
                printf("Error Fork\r\n"); 
                exit(0);
            }
            
            if(direwolf_pid == 0){startDirewolf();}
            
            sleep(1);
            int direwolf_fd = createClient("127.0.0.1", 8001);
            i = 0;
            while(i < 40){
                sleep(1);
                switch(uhfvhfMode){
                    case TX:
                        transmitVHFUHF(direwolf_fd, "Helloooooooooooooooooooooooo"); 
                        uhfvhfMode = RX;
                    break;
                    case RX:
                        if(waitForUplink){
                          if((msgSize = receiveVHFUHF(direwolf_fd, msg)) != -1){
                           // if(compareArray(msg, UHFVHF_REQUEST, msgSize)){
                                //frame_decon_vhf(buffer, nBytesRead, 1, ERROR_FRAME);
                                printf("UPLINK RECIEVED\r\n");
                                waitForUplink = 1;
                                uhfvhfMode = TX;
                            //}
                         }
                        }
                                               
                    break;
                    
                }
                i++;
            }
            close(direwolf_fd);
            stopDirewolf(direwolf_pid);
            usleep(500000);
            stop_rigctld();
        
    }
    
    return(0);
}


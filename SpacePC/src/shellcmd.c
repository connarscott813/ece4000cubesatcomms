#include "shellcmd.h"

#define BUFFER_SIZE 256

void executeCmd(char ** cmd, uint32_t cmdSize){

 pid_t pid;
 pid_t child = 0;
 pid_t error = -1;
 
 fflush(stdout);
 
 if(DEBUG_SHELLCMD)printf("Entering fork\n\r");

 pid = forkProc();
 if(pid != error){
    if(pid != child){ //parent process
        if(DEBUG_SHELLCMD)printf("parent wait %d\n\r", getpid());
        waitProc(pid); // wait for child process
        if(DEBUG_SHELLCMD)printf("parent done wait\n\r");
         }
    else { //child process
        if(DEBUG_SHELLCMD)fprintf(stderr,"in child %d\n\r", getpid());
        char *redirectLoc;
        if((redirectLoc = checkRedirect(cmd, cmdSize)) != NULL){
            executeRedirect(cmd, cmdSize, redirectLoc);
            }
        else{
            execute(cmd, cmdSize);
            exit(1);
            }
        }

    }
}


pid_t forkProc(void){
    pid_t pid;
    if((pid = fork()) == -1){ perror("error fork\n\r");}
    return(pid);
}

void waitProc(pid_t pid){
    int status, retVal;
        if((retVal = waitpid(pid, &status, 0) ) > 0) {
            if(DEBUG_SHELLCMD){
                printf("Parent: Child done.\n");
                printf("Return value: %d\n", retVal);
                printf("Status:       %d\n", status);
                printf("WIFSTOPPED:   %d\n", WIFSTOPPED(status));
                printf("WIFSIGNALED:  %d\n", WIFSIGNALED(status));
                printf("WIFEXITED:    %d\n", WIFEXITED(status));
                printf("WEXITSTATUS:  %d\n", WEXITSTATUS(status));
                printf("WTERMSIG:     %d\n", WTERMSIG(status));
                printf("WSTOPSIG:     %d\n", WSTOPSIG(status));
            }
        }
}


void execute(char **cmd, uint32_t cmdSize){
    
    if(DEBUG_SHELLCMD){//display commands to shell
        uint32_t i = 0;
        while(cmd[i] != NULL){ 
            printf(" %s", cmd[i]);
            i++;
        }
        printf("\n");
    }
    
    int returnVal = execvp(cmd[0], cmd);
    if(returnVal == -1) printf("Error executing %s", cmd[0]);

    
        exit(0); //kill child proc
}

char * checkRedirect(char **cmd, uint32_t cmdSize){
    uint32_t i = 0;
    while(cmd[i] != NULL){
        if(!strcmp(cmd[i],">")){
            return(cmd[i+1]);
        }
        i++;
    }
    return(NULL);
}

void executeRedirect(char **cmd, uint32_t cmdSize, char *redirectLoc){
        int fd; 
        int stdout_saved;
        pid_t child = 0;
        pid_t error = -1;
        if(DEBUG_SHELLCMD){printf("in executeRedirect\n\r");}
        
        if((fd = open(redirectLoc, O_RDWR|O_CREAT)) == -1){
             printf("Error open()\n\r");   
            }
        else{
            stdout_saved = dup(1); //save stdout
            dup2(fd,1); //redirect stdout to fd
            close(fd);
            pid_t pid = forkProc();
            if(pid != error){
                if(pid != child){ //parent process
                    if(DEBUG_SHELLCMD)printf("parent wait %d\n\r", getpid());
                    dup2(stdout_saved, 1); //return stdout
                    close(stdout_saved);
                    waitProc(pid); // wait for child process
                    if(DEBUG_SHELLCMD)printf("parent done wait\n\r");
                    exit(0);
                    }
                else { //child process
                    if(DEBUG_SHELLCMD)fprintf(stderr,"in child %d\n\r", getpid());
                    execute(cmd, cmdSize);
                    exit(1);
                    }

                }
            }
}

void createPipe(pipefd_t pipefd){
  if(pipe(pipefd) == -1){
    perror("error creating pipe");
    exit(EXIT_FAILURE);
  }

}

void closePipeEnd(pipeCmd_E pipeOp, pipefd_t pipefd){

      switch(pipeOp){

        case READ:
            if(close(pipefd[READ]) == -1){
                perror("Error close pipe READ end\r\n");
            } 
            break;
            
        case WRITE:
            if(close(pipefd[WRITE]) == -1){
                perror("Error close pipe WRITE end\r\n");
            } 
            break;

  }

}

int pipeOperation(pipeCmd_E pipeOp, pipefd_t pipefd, char argv[],  int argc){
    int returnVal;
  switch(pipeOp){

      case READ: 
          returnVal = read(pipefd[READ], argv, argc); 
            if(returnVal == -1){
                perror("error reading file: pipe");
                exit(EXIT_FAILURE);
                }

        break;

      case WRITE:
            returnVal = write(pipefd[WRITE], argv, argc);
            if(( returnVal == -1)){
                perror("error writing to file: pipe");
                exit(EXIT_FAILURE);
            }
        break;

  }
    return(returnVal);
}

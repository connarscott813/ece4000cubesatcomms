/**
 * @file gs_reception.h
 * @author Connar Scott
 * @brief Ground Station reception algorithms and some
 *        array util functions for evaluating recieved
 *        messages
 */
#ifndef _GS_RECEPTION_H_
#define _GS_RECEPTION_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <czmq.h>
#include <zmq.h>
#include <sys/socket.h>

#include "timecmpr.h"
#include "shellcmd.h"
#include "getPassInfo.h"
#include "frames.h"

#define REQUEST_TIMEOUT 200
#define REP_SERVER "tcp://127.0.0.1:5557"
#define REQ_RETRIES 1

typedef struct processId {
    pid_t sbandrxPID;
    pid_t sbandtxPID;
}processId_t;

/**
 * Initializes the Req client for zeroMQ
 * @return None
 */
void zeroMQ_ReqInit(void);

/**
 * Checks for a message from the REP server, and fills
 * the buffer with a message and sets length to the length
 * of the message. This is a non-blocking function
 * @param buffer - buffer to put message in
 * @param length - pointer to length var to set
 * @return true if message exists
 */
bool zeroMQ_ReqRead(unsigned char * buffer, unsigned int * length);

/**
 * IDestroys the Req client for zeroMQ
 * @return None
 */
void zeroMQ_Destroy(void);

/**
 * Returns the length of the input array
 * @param array[] - input array
 * @param maxArraySize - the max array size
 * @return length of the array as an integer
 */
int getArrayLength(char array[], int maxArraySize);

/**
 * Prints the contents of the array to stdout
 * @param array - pointer to array
 * @param size - size of array
 * @return None
 */
void printArray(unsigned char * array, uint32_t size);

/**
 *Compares the contents of the input arrays
 * @param array1 - input array 1
 * @param array2 - input array 2
 * @param size - max size of array
 * @return True if arrays are equal
 */
bool compareArray(unsigned char * array1, unsigned char * array2, uint32_t size);

/**
 * Checks for a recieved message from the direwolf server,
 * if there is a message it is returned in the rec var
 * @param fd - file descriptor for direwolf tcp socket
 * @param rec - pointer to char array to store message
 * @return number of bytes read
 */
int receiveVHFUHF(int fd, char *rec);

#endif

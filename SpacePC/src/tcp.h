/**
 * @file tcp.h
 * @author Connar Scott
 * @brief Functions for creating tcp client/server
 */


#ifndef _TCP_H_
#define _TCP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define BACKLOG_QUEUE_SIZE 256
#define handle_error(msg) \
    do{perror(msg); exit(EXIT_FAILURE);}while(0)

/**
 * Creates a server using address and port input
 * @param address - server address
 * @param port - server port
 * @return None
 */
int createServer(const char * address, int port);

/**
 * Creates a client and connects to server
 * @param address - server address
 * @param port - server port
 * @return None
 */
int createClient(const char * address, int port);

#endif

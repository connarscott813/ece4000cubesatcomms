/**
 * @file getPassInfo.h
 * @author Connar Scott, Brett Robertson
 * @brief Functions for managing folders and writing/reading to files
 */
#ifndef _FILE_UTIL_H_
#define _FILE_UTIL_H_

#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/dir.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "shellcmd.h"
#include "queue.h"

#define DEBUG_FILE_UTIL 1

/**
 * Initializes the current directory in the internal curdir 
 * variable
 * @return None
 */
void initCurDir(void);

/**
 * Returns the current folder number, retrieved from the 
 * foldernum.bin file in bin
 * @return The folder number
 */
uint32_t getFolderNum(void);

/**
 * Writes the foldernum variable back to the folernum.bin file
 * @return None
 */
void writeFolderNum(void);

/**
 * Moves a folder and its contesnt to the destination
 * @param srcFolderDir - string containing path to src folder
 * @param destFolderDir - string containing path to dest location
 * @return None
 */
void moveFolder(char * srcFolderDir, char * destFolderDir);

/**
 * Creates a folder
 * @param folderName - string containing folder name
 * @param folderPath - string containing path to dest location
 * @return None
 */
void createFolder(char * folderName, char * folderPath);

/**
 * Stores data to bin file
 * @param PacketData - array of chars to write
 * @param datasize - size of data
 * @param PATH - path to bin file
 * @return None
 */
void storeData(unsigned char PacketData[], uint32_t datasize, const char* PATH);

/**
 * Writes data to bin file
 * @param PacketData - array of chars to write
 * @param datasize - size of data
 * @param PATH - path to bin file
 * @return None
 */
void writeData(unsigned char PacketData[], uint32_t datasize, char* PATH);

/**
 * Returns number of lines in a bin file
 * @param PATH - path to bin file
 * @return number of lines in bin file
 */
uint32_t sizeData(const char* PATH);

/**
 * Initializes folders for storing windows/frames.
 * @param srcFolderDir - path to src folder
 * @return None
 */
void initFileManagement(char * srcFolderDir);

/**
 * Writes the packet contents to a file denoted by the
 * pscn number
 * @param path - path to create file
 * @param pscn - packet identifier
 * @param msg - char array message
 * @param msgSize - size of message
 * @return None
 */
void writePSCN2File(const char * path, unsigned int pscn, unsigned char msg[], uint32_t msgSize);


/**
 * Moves the folder in the srcFolderDir to destFolderDir and creates
 * a new folder in place of the old srcFolderDir but increments the
 * folder number based on the window count.
 * @param srcFolderDir - source folder path
 * @param destFolderDir - destination path
 * @return None
 */
void moveNcreateWindowFolder(char * srcFolderDir, char * destFolderDir);

/**
 * checks and fills the uplinkCmdQ based on the user commands 
 * in the assocaited .bin file
 * @param uplinkCmdQ - a queue for strings
 * @param uplinkFilePath - path to uplink commands file
 * @return None
 */
void checkUserUplinkCommands(string_queue_t * uplinkCmdQ, char * uplinkFilePath);


#endif

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Rx VIOLET (LimeSDR Mini)
# Author: Thomas Boyle
# Description: Flowgraph to received a file with the LimeSDR Mini over S-band
# Generated: Sat Mar 12 15:49:45 2022
##################################################


from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import zeromq
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from grc_gnuradio import blks2 as grc_blks2
from optparse import OptionParser
import limesdr


class rx_violet(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Rx VIOLET (LimeSDR Mini)")

        ##################################################
        # Variables
        ##################################################
        self.sps = sps = 2
        self.sdr_gain = sdr_gain = 50
        self.samp_rate = samp_rate = 4e6
        self.centre_freq = centre_freq = 2.4054e9

        ##################################################
        # Blocks
        ##################################################
        self.zeromq_rep_sink_0 = zeromq.rep_sink(gr.sizeof_char, 1, 'tcp://127.0.0.1:5557', 100, False, 1024)
        self.low_pass_filter_0 = filter.fir_filter_ccf(1, firdes.low_pass(
        	1, samp_rate, samp_rate/2, samp_rate/10, firdes.WIN_HAMMING, 6.76))
        self.limesdr_source_0 = limesdr.source('1D7514D4CD338E', 0, '')
        self.limesdr_source_0.set_sample_rate(samp_rate)
        self.limesdr_source_0.set_center_freq(centre_freq, 0)
        self.limesdr_source_0.set_bandwidth(5e6,0)
        self.limesdr_source_0.set_gain(sdr_gain,0)
        self.limesdr_source_0.set_antenna(255,0)
        self.limesdr_source_0.calibrate(5e6, 0)

        self.digital_msk_timing_recovery_cc_0 = digital.msk_timing_recovery_cc(sps, 0.05, 0.1, sps)
        self.digital_gmsk_demod_0 = digital.gmsk_demod(
        	samples_per_symbol=sps,
        	gain_mu=0.175,
        	mu=0.5,
        	omega_relative_limit=0.005,
        	freq_error=1e-4,
        	verbose=False,
        	log=False,
        )
        self.blks2_packet_decoder_0 = grc_blks2.packet_demod_b(grc_blks2.packet_decoder(
        		access_code='',
        		threshold=-1,
        		callback=lambda ok, payload: self.blks2_packet_decoder_0.recv_pkt(ok, payload),
        	),
        )



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blks2_packet_decoder_0, 0), (self.zeromq_rep_sink_0, 0))
        self.connect((self.digital_gmsk_demod_0, 0), (self.blks2_packet_decoder_0, 0))
        self.connect((self.digital_msk_timing_recovery_cc_0, 0), (self.digital_gmsk_demod_0, 0))
        self.connect((self.limesdr_source_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.digital_msk_timing_recovery_cc_0, 0))

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.digital_msk_timing_recovery_cc_0.set_sps(self.sps)

    def get_sdr_gain(self):
        return self.sdr_gain

    def set_sdr_gain(self, sdr_gain):
        self.sdr_gain = sdr_gain
        self.limesdr_source_0.set_gain(self.sdr_gain,0)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, self.samp_rate/2, self.samp_rate/10, firdes.WIN_HAMMING, 6.76))

    def get_centre_freq(self):
        return self.centre_freq

    def set_centre_freq(self, centre_freq):
        self.centre_freq = centre_freq
        self.limesdr_source_0.set_center_freq(self.centre_freq, 0)


def main(top_block_cls=rx_violet, options=None):

    tb = top_block_cls()
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()

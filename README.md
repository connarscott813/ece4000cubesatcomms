# CubeSat NB Groundstation Software

## Description
Groundstation software for interaction between:
- LimeSDR and LimeSDR mini using GNU Radio python scripts
- ICOM9700 and YAESU radios using Direwolf
- Gpredict and the MD-01 for rotation of Az/El BIG RAS Rotator (Rotator control is managed by gpredict)

Commands to be uplinked can be inputted using a txt file. Every window recieved is written to disk with a name corresponding the the window number.

The software is set up to receive the selected satellite's pass data from gpredict, which is used to schedule and manage the transmission/reception pattern based on the current elevation.

## State of Implementation
Currently the code is set up to demonstrate VHF/UHF communications. To change to S-Band the config.h file must be edited in GroundStation/VIOLETpass and SpacePC

The gpredict file path is set to a test file that exists in the Ground PC's '/data' folder. For this to retrieve real data from gpredict, the text file must be changed to '/data/gpredict_next_pass.txt' in the config file. 'gpredict_next_pass.txt' will update its contents based on the selected satellite in gpredict on the Ground PC.

It should be noted that scheduleAOS.exe uses the at daemon which has no terminal output upon execution. To see an output a log file should be used.

## Installation
The software is only compatible with Linux OS. More specifically, it has been designed for the Ground PC and Space PC in GWE-137.

To install the following steps must be performed:
1. Enter 'GroundStation/scheduleAOS' directory and open a terminal. Use the command 'make'
2. Enter 'GroundStation/VIOLETpass' directory and open a terminal. Use the command 'make'
3. Enter 'GroundStation' directory and open a terminal. Use the command 'make'
4. The executable 'groundstation' should be in the 'GroundStation' directory.

## Future Development
The following subdirectories can be used for development: GroundStation, VIOLETpass, and scheduleAOS. Makefiles are used to compile code in C using the gcc compiler. The following commands are currently implemented:

1. 'make' - compiles the files in the 'src' folder, creates and puts the create object files in the 'obj' folder, and creates the executable file.
2. 'make clean' - removes all object files and deletes the 'obj' folder and the executable file.
3. 'make run' - Runs the executable file.

## Troubleshooting
For all troubleshooting first refer to the User's Guide to verify proper set-up of all equipment.

Restarting the PC should solve most issues. 

Someone developing the code should have an understanding of how to kill processes: The most common issue during development is processes not closing themselves properly due to the code not exiting gracefully.

## Additional Information
More information is availible in the Capstone Final Design Document. This document outlines the data structures and their uses, a short description of C modules and their dependencies, and an explanation of how the code works as well as future recommendations.

## Authors
Connar Scott<br />
Brett Robertson<br />
Ethan Brewer<br />
Thomas Boyle<br />

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: File Tx Example
# Author: Thomas
# Description: Example of the File Source and File Sink functionality
# Generated: Fri Feb 18 18:44:54 2022
##################################################


from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import pmt


class file_rx_ex(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "File Tx Example")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 32000

        ##################################################
        # Blocks
        ##################################################
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_char*1, samp_rate,True)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, 'C:\\Users\\Guard\\Documents\\University\\4th Year\\ENGG4000\\CubeSat-NB\\Team-Repo\\ece4000cubesatcomms\\SDRDevelopment\\File-Tx-Example\\sample_input.txt', True)
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_char*1, 'C:\\Users\\Guard\\Documents\\University\\4th Year\\ENGG4000\\CubeSat-NB\\Team-Repo\\ece4000cubesatcomms\\SDRDevelopment\\File-Tx-Example\\sample_output.txt', True)
        self.blocks_file_sink_0.set_unbuffered(False)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_file_source_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.blocks_file_sink_0, 0))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)


def main(top_block_cls=file_rx_ex, options=None):

    tb = top_block_cls()
    tb.start(10)
    tb.wait()


if __name__ == '__main__':
    main()

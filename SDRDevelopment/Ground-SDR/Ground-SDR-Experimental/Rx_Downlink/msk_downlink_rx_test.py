#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: MSK Downlink Rx Test
# Author: Thomas Boyle
# Description: Flowgraph to Test MSK Downlink Rx without the Packet Encoder
# Generated: Wed Mar  9 16:30:06 2022
##################################################


from gnuradio import analog
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import limesdr
import math
import nrzi_decoder_0


class msk_downlink_rx_test(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "MSK Downlink Rx Test")

        ##################################################
        # Variables
        ##################################################
        self.baud_rate = baud_rate = 800e3
        self.samp_rate = samp_rate = 4e6
        self.del_f = del_f = baud_rate/2
        self.sps = sps = 5
        self.sdr_gain = sdr_gain = 50
        self.repeat = repeat = int(samp_rate/baud_rate)
        self.packet_len = packet_len = 90
        self.demod_gain = demod_gain = samp_rate/(2*math.pi*del_f)
        self.centre_freq = centre_freq = 2.4054e9
        self.cal_bw = cal_bw = 5e6

        ##################################################
        # Blocks
        ##################################################
        self.nrzi_decoder_0 = nrzi_decoder_0.blk()
        self.limesdr_source_0 = limesdr.source('0009083401881019', 0, '')
        self.limesdr_source_0.set_sample_rate(samp_rate)
        self.limesdr_source_0.set_center_freq(centre_freq, 0)
        self.limesdr_source_0.set_bandwidth(5e6,0)
        self.limesdr_source_0.set_gain(sdr_gain,0)
        self.limesdr_source_0.set_antenna(1,0)
        self.limesdr_source_0.calibrate(cal_bw, 0)

        self.digital_hdlc_deframer_bp_0 = digital.hdlc_deframer_bp(32, 500)
        self.digital_clock_recovery_mm_xx_0 = digital.clock_recovery_mm_ff(repeat, 0.25*0.175*0.175, 0.5, 0.175, 0.005)
        self.digital_binary_slicer_fb_0 = digital.binary_slicer_fb()
        self.blocks_pdu_to_tagged_stream_0 = blocks.pdu_to_tagged_stream(blocks.byte_t, 'packet_len')
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_char*1, '/home/remote/ece4000cubesatcomms/SDRDevelopment/Ground-SDR/Ground-SDR-Experimental/Rx_Downlink/packet_rx.txt', False)
        self.blocks_file_sink_0.set_unbuffered(True)
        self.analog_quadrature_demod_cf_0 = analog.quadrature_demod_cf(demod_gain)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.digital_hdlc_deframer_bp_0, 'out'), (self.blocks_pdu_to_tagged_stream_0, 'pdus'))
        self.connect((self.analog_quadrature_demod_cf_0, 0), (self.digital_clock_recovery_mm_xx_0, 0))
        self.connect((self.blocks_pdu_to_tagged_stream_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.digital_binary_slicer_fb_0, 0), (self.nrzi_decoder_0, 0))
        self.connect((self.digital_clock_recovery_mm_xx_0, 0), (self.digital_binary_slicer_fb_0, 0))
        self.connect((self.limesdr_source_0, 0), (self.analog_quadrature_demod_cf_0, 0))
        self.connect((self.nrzi_decoder_0, 0), (self.digital_hdlc_deframer_bp_0, 0))

    def get_baud_rate(self):
        return self.baud_rate

    def set_baud_rate(self, baud_rate):
        self.baud_rate = baud_rate
        self.set_repeat(int(self.samp_rate/self.baud_rate))
        self.set_del_f(self.baud_rate/2)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_repeat(int(self.samp_rate/self.baud_rate))
        self.set_demod_gain(self.samp_rate/(2*math.pi*self.del_f))

    def get_del_f(self):
        return self.del_f

    def set_del_f(self, del_f):
        self.del_f = del_f
        self.set_demod_gain(self.samp_rate/(2*math.pi*self.del_f))

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps

    def get_sdr_gain(self):
        return self.sdr_gain

    def set_sdr_gain(self, sdr_gain):
        self.sdr_gain = sdr_gain
        self.limesdr_source_0.set_gain(self.sdr_gain,0)

    def get_repeat(self):
        return self.repeat

    def set_repeat(self, repeat):
        self.repeat = repeat
        self.digital_clock_recovery_mm_xx_0.set_omega(self.repeat)

    def get_packet_len(self):
        return self.packet_len

    def set_packet_len(self, packet_len):
        self.packet_len = packet_len

    def get_demod_gain(self):
        return self.demod_gain

    def set_demod_gain(self, demod_gain):
        self.demod_gain = demod_gain
        self.analog_quadrature_demod_cf_0.set_gain(self.demod_gain)

    def get_centre_freq(self):
        return self.centre_freq

    def set_centre_freq(self, centre_freq):
        self.centre_freq = centre_freq
        self.limesdr_source_0.set_center_freq(self.centre_freq, 0)

    def get_cal_bw(self):
        return self.cal_bw

    def set_cal_bw(self, cal_bw):
        self.cal_bw = cal_bw


def main(top_block_cls=msk_downlink_rx_test, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Ground Station SDR Tx/Rx
# Author: Thomas Boyle
# Description: Combined Tx + Rx flowgraph to control the LimeSDR on the Ground PC.
# Generated: Sat Mar  5 16:10:38 2022
##################################################


from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from grc_gnuradio import blks2 as grc_blks2
from optparse import OptionParser
import limesdr
import pmt
import time


class ground_sdr_trx_nogui(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Ground Station SDR Tx/Rx")
        startTime = time.time()
        ##################################################
        # Variables
        ##################################################
        self.sps = sps = 2
        self.sdr_gain = sdr_gain = 50
        self.samp_rate = samp_rate = 4e6
        self.centre_freq = centre_freq = 2.4054e9
        self.cal_bw = cal_bw = 0

        ##################################################
        # Blocks
        ##################################################
        self.low_pass_filter_0 = filter.fir_filter_ccf(1, firdes.low_pass(
        	1, samp_rate, samp_rate/2, samp_rate/10, firdes.WIN_HAMMING, 6.76))
        self.limesdr_source_0 = limesdr.source('0009083401881019', 0, '')
        self.limesdr_source_0.set_sample_rate(samp_rate)
        self.limesdr_source_0.set_center_freq(centre_freq, 0)
        self.limesdr_source_0.set_bandwidth(5e6,0)
        self.limesdr_source_0.set_gain(sdr_gain,0)
        self.limesdr_source_0.set_antenna(1,0)

        self.limesdr_sink_0 = limesdr.sink('0009083401881019', 0, '', '')
        self.limesdr_sink_0.set_sample_rate(samp_rate)
        self.limesdr_sink_0.set_center_freq(centre_freq, 0)
        self.limesdr_sink_0.set_bandwidth(5e6,0)
        self.limesdr_sink_0.set_gain(sdr_gain,0)
        self.limesdr_sink_0.set_antenna(255,0)

        self.digital_msk_timing_recovery_cc_0 = digital.msk_timing_recovery_cc(sps, 0.05, 0.1, sps)
        self.digital_gmsk_mod_0 = digital.gmsk_mod(
        	samples_per_symbol=sps,
        	bt=0.25,
        	verbose=False,
        	log=False,
        )
        self.digital_gmsk_demod_0 = digital.gmsk_demod(
        	samples_per_symbol=sps,
        	gain_mu=0.175,
        	mu=0.5,
        	omega_relative_limit=0.005,
        	freq_error=1e-4,
        	verbose=False,
        	log=False,
        )
        self.blocks_head_0 = blocks.head(gr.sizeof_char*1, 174)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, '/home/remote/ece4000cubesatcomms/SDRDevelopment/Ground-SDR-v1/Loopback_NoGUI/loopback_tx.txt', True)
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_char*1, '/home/remote/ece4000cubesatcomms/SDRDevelopment/Ground-SDR-v1/Loopback_NoGUI/loopback_rx.txt', False)
        self.blocks_file_sink_0.set_unbuffered(True)
        self.blks2_packet_encoder_0 = grc_blks2.packet_mod_b(grc_blks2.packet_encoder(
        		samples_per_symbol=sps,
        		bits_per_symbol=1,
        		preamble='',
        		access_code='',
        		pad_for_usrp=True,
        	),
        	payload_length=174,
        )
        self.blks2_packet_decoder_0 = grc_blks2.packet_demod_b(grc_blks2.packet_decoder(
        		access_code='',
        		threshold=-1,
        		callback=lambda ok, payload: self.blks2_packet_decoder_0.recv_pkt(ok, payload),
        	),
        )



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blks2_packet_decoder_0, 0), (self.blocks_head_0, 0))
        self.connect((self.blks2_packet_encoder_0, 0), (self.digital_gmsk_mod_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blks2_packet_encoder_0, 0))
        self.connect((self.blocks_head_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.digital_gmsk_demod_0, 0), (self.blks2_packet_decoder_0, 0))
        self.connect((self.digital_gmsk_mod_0, 0), (self.limesdr_sink_0, 0))
        self.connect((self.digital_msk_timing_recovery_cc_0, 0), (self.digital_gmsk_demod_0, 0))
        self.connect((self.limesdr_source_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.digital_msk_timing_recovery_cc_0, 0))
        
        endTime = time.time()
        print "Executed Time Is:"
        print (endTime-startTime)

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.digital_msk_timing_recovery_cc_0.set_sps(self.sps)

    def get_sdr_gain(self):
        return self.sdr_gain

    def set_sdr_gain(self, sdr_gain):
        self.sdr_gain = sdr_gain
        self.limesdr_source_0.set_gain(self.sdr_gain,0)
        self.limesdr_sink_0.set_gain(self.sdr_gain,0)
        self.limesdr_sink_0.set_gain(self.sdr_gain,1)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, self.samp_rate/2, self.samp_rate/10, firdes.WIN_HAMMING, 6.76))

    def get_centre_freq(self):
        return self.centre_freq

    def set_centre_freq(self, centre_freq):
        self.centre_freq = centre_freq
        self.limesdr_source_0.set_center_freq(self.centre_freq, 0)
        self.limesdr_sink_0.set_center_freq(self.centre_freq, 0)

    def get_cal_bw(self):
        return self.cal_bw

    def set_cal_bw(self, cal_bw):
        self.cal_bw = cal_bw


def main(top_block_cls=ground_sdr_trx_nogui, options=None):

    tb = top_block_cls()
    tb.start()
    time.sleep(0.1)
    tb.stop()


if __name__ == '__main__':
    main()

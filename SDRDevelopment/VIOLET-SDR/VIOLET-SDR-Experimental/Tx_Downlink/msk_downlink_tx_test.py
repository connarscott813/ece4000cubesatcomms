#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: MSK Downlink Tx Test
# Author: Thomas Boyle
# Description: Flowgraph to Test MSK Downlink Tx without the Packet Encoder
# Generated: Wed Mar  9 16:16:38 2022
##################################################


from gnuradio import analog
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import limesdr
import math
import nrzi_encoder_0
import pmt


class msk_downlink_tx_test(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "MSK Downlink Tx Test")

        ##################################################
        # Variables
        ##################################################
        self.baud_rate = baud_rate = 800e3
        self.samp_rate = samp_rate = 4e6
        self.del_f = del_f = baud_rate/2
        self.sps = sps = 5
        self.sensitivity = sensitivity = (2*math.pi*del_f)/(samp_rate)
        self.sdr_gain = sdr_gain = 50
        self.repeat = repeat = int(samp_rate/baud_rate)
        self.packet_len = packet_len = 90
        self.centre_freq = centre_freq = 2.4054e9
        self.cal_bw = cal_bw = 5e6

        ##################################################
        # Blocks
        ##################################################
        self.nrzi_encoder_0 = nrzi_encoder_0.blk()
        self.limesdr_sink_0 = limesdr.sink('1D7514D4CD338E', 0, '', '')
        self.limesdr_sink_0.set_sample_rate(samp_rate)
        self.limesdr_sink_0.set_center_freq(centre_freq, 0)
        self.limesdr_sink_0.set_bandwidth(5e6,0)
        self.limesdr_sink_0.set_gain(sdr_gain,0)
        self.limesdr_sink_0.set_antenna(255,0)
        self.limesdr_sink_0.calibrate(cal_bw, 0)

        self.digital_hdlc_framer_pb_0 = digital.hdlc_framer_pb('packet_len')
        self.blocks_tagged_stream_to_pdu_0 = blocks.tagged_stream_to_pdu(blocks.byte_t, 'packet_len')
        self.blocks_stream_to_tagged_stream_0 = blocks.stream_to_tagged_stream(gr.sizeof_char, 1, packet_len, 'packet_len')
        self.blocks_repeat_0 = blocks.repeat(gr.sizeof_char*1, repeat)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, '/home/remote/ece4000cubesatcomms/SDRDevelopment/VIOLET-SDR/VIOLET-SDR-Experimental/Tx_Downlink/packet_tx.txt', False)
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_char_to_float_0 = blocks.char_to_float(1, 1)
        self.analog_frequency_modulator_fc_0 = analog.frequency_modulator_fc(sensitivity)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_tagged_stream_to_pdu_0, 'pdus'), (self.digital_hdlc_framer_pb_0, 'in'))
        self.connect((self.analog_frequency_modulator_fc_0, 0), (self.limesdr_sink_0, 0))
        self.connect((self.blocks_char_to_float_0, 0), (self.analog_frequency_modulator_fc_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_stream_to_tagged_stream_0, 0))
        self.connect((self.blocks_repeat_0, 0), (self.blocks_char_to_float_0, 0))
        self.connect((self.blocks_stream_to_tagged_stream_0, 0), (self.blocks_tagged_stream_to_pdu_0, 0))
        self.connect((self.digital_hdlc_framer_pb_0, 0), (self.nrzi_encoder_0, 0))
        self.connect((self.nrzi_encoder_0, 0), (self.blocks_repeat_0, 0))

    def get_baud_rate(self):
        return self.baud_rate

    def set_baud_rate(self, baud_rate):
        self.baud_rate = baud_rate
        self.set_repeat(int(self.samp_rate/self.baud_rate))
        self.set_del_f(self.baud_rate/2)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_sensitivity((2*math.pi*self.del_f)/(self.samp_rate))
        self.set_repeat(int(self.samp_rate/self.baud_rate))

    def get_del_f(self):
        return self.del_f

    def set_del_f(self, del_f):
        self.del_f = del_f
        self.set_sensitivity((2*math.pi*self.del_f)/(self.samp_rate))

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps

    def get_sensitivity(self):
        return self.sensitivity

    def set_sensitivity(self, sensitivity):
        self.sensitivity = sensitivity
        self.analog_frequency_modulator_fc_0.set_sensitivity(self.sensitivity)

    def get_sdr_gain(self):
        return self.sdr_gain

    def set_sdr_gain(self, sdr_gain):
        self.sdr_gain = sdr_gain
        self.limesdr_sink_0.set_gain(self.sdr_gain,0)

    def get_repeat(self):
        return self.repeat

    def set_repeat(self, repeat):
        self.repeat = repeat
        self.blocks_repeat_0.set_interpolation(self.repeat)

    def get_packet_len(self):
        return self.packet_len

    def set_packet_len(self, packet_len):
        self.packet_len = packet_len
        self.blocks_stream_to_tagged_stream_0.set_packet_len(self.packet_len)
        self.blocks_stream_to_tagged_stream_0.set_packet_len_pmt(self.packet_len)

    def get_centre_freq(self):
        return self.centre_freq

    def set_centre_freq(self, centre_freq):
        self.centre_freq = centre_freq
        self.limesdr_sink_0.set_center_freq(self.centre_freq, 0)

    def get_cal_bw(self):
        return self.cal_bw

    def set_cal_bw(self, cal_bw):
        self.cal_bw = cal_bw


def main(top_block_cls=msk_downlink_tx_test, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()

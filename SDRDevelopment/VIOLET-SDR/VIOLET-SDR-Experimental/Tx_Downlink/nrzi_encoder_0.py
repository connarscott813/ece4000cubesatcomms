"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!

Author: Thomas Boyle
Date: March 9, 2022
"""

import numpy as np
from gnuradio import gr


class blk(gr.sync_block):  # other base classes are basic_block, decim_block, interp_block
    """Embedded Python Block example - Non-return-to-zero Encoder
    This block expects a binary signal which varies between values of 0 and 1
    It changes the output to vary between values of -1 and 1."""

    def __init__(self):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.sync_block.__init__(
            self,
            name='NRZI Encoder',   # will show up in GRC
            in_sig=[np.byte],
            out_sig=[np.byte]
        )
    def work(self, input_items, output_items):
        """Implement NRZI Encoding"""
        output_items[0][:] = ((input_items[0] * 2)-1)
        return len(output_items[0])

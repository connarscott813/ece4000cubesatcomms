#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Experimental MSK Loopback (On LimeSDR)
# Author: Thomas Boyle
# Description: Flowgraph to test an MSK loopback without the Packet Encoder/Decoder on the LimeSDR
# Generated: Fri Mar 11 15:39:24 2022
##################################################


from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import pmt


class experimental_msk_loopback_hw(gr.top_block):

    def __init__(self, hdr_format=digital.header_format_default(digital.packet_utils.default_access_code, 0)):
        gr.top_block.__init__(self, "Experimental MSK Loopback (On LimeSDR)")

        ##################################################
        # Parameters
        ##################################################
        self.hdr_format = hdr_format

        ##################################################
        # Variables
        ##################################################
        self.sps = sps = 2
        self.sdr_gain = sdr_gain = 50
        self.samp_rate = samp_rate = 4e6
        self.packet_len = packet_len = 10
        self.length_tag_key = length_tag_key = "packet_len"
        self.hsize = hsize = 8
        self.centre_freq = centre_freq = 2.4054e9
        self.cal_bw = cal_bw = 5e6

        ##################################################
        # Blocks
        ##################################################
        self.digital_packet_headerparser_b_default_0 = digital.packet_headerparser_b(hsize, "packet_len")
        self.digital_packet_headergenerator_bb_default_0 = digital.packet_headergenerator_bb(hsize, length_tag_key)
        self.digital_header_payload_demux_0 = digital.header_payload_demux(
        	  hsize,
        	  1,
        	  0,
        	  length_tag_key,
        	  length_tag_key,
        	  False,
        	  gr.sizeof_char,
        	  '',
                  int(samp_rate),
                  (),
                  0,
            )
        self.digital_gmsk_mod_0 = digital.gmsk_mod(
        	samples_per_symbol=sps,
        	bt=0.35,
        	verbose=False,
        	log=False,
        )
        self.digital_gmsk_demod_0 = digital.gmsk_demod(
        	samples_per_symbol=2,
        	gain_mu=0.175,
        	mu=0.5,
        	omega_relative_limit=0.005,
        	freq_error=0.0,
        	verbose=False,
        	log=False,
        )
        self.blocks_tagged_stream_mux_0 = blocks.tagged_stream_mux(gr.sizeof_char*1, length_tag_key, 0)
        self.blocks_tag_debug_0_0_0 = blocks.tag_debug(gr.sizeof_char*1, 'Demod Data', ""); self.blocks_tag_debug_0_0_0.set_display(True)
        self.blocks_tag_debug_0_0 = blocks.tag_debug(gr.sizeof_char*1, 'Mod Data', ""); self.blocks_tag_debug_0_0.set_display(True)
        self.blocks_tag_debug_0 = blocks.tag_debug(gr.sizeof_char*1, 'Rx Bytes', ""); self.blocks_tag_debug_0.set_display(True)
        self.blocks_stream_to_tagged_stream_0 = blocks.stream_to_tagged_stream(gr.sizeof_char, 1, packet_len, length_tag_key)
        self.blocks_repack_bits_bb_1 = blocks.repack_bits_bb(1, 8, length_tag_key, False, gr.GR_LSB_FIRST)
        self.blocks_repack_bits_bb_0 = blocks.repack_bits_bb(8, 1, length_tag_key, False, gr.GR_LSB_FIRST)
        self.blocks_pack_k_bits_bb_0 = blocks.pack_k_bits_bb(8)
        self.blocks_null_source_0 = blocks.null_source(gr.sizeof_char*1)
        self.blocks_head_0 = blocks.head(gr.sizeof_char*1, 1000)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, '/home/remote/ece4000cubesatcomms/SDRDevelopment/Testing/LimeSDR_Experimental_MSK_Loopback/packet_tx.txt', False)
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_char*1, '/home/remote/ece4000cubesatcomms/SDRDevelopment/Testing/LimeSDR_Experimental_MSK_Loopback/packet_rx.txt', False)
        self.blocks_file_sink_0.set_unbuffered(True)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.digital_packet_headerparser_b_default_0, 'header_data'), (self.digital_header_payload_demux_0, 'header_data'))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_stream_to_tagged_stream_0, 0))
        self.connect((self.blocks_head_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.blocks_null_source_0, 0), (self.digital_header_payload_demux_0, 1))
        self.connect((self.blocks_pack_k_bits_bb_0, 0), (self.digital_gmsk_mod_0, 0))
        self.connect((self.blocks_repack_bits_bb_0, 0), (self.blocks_tagged_stream_mux_0, 1))
        self.connect((self.blocks_repack_bits_bb_0, 0), (self.digital_packet_headergenerator_bb_default_0, 0))
        self.connect((self.blocks_repack_bits_bb_1, 0), (self.blocks_head_0, 0))
        self.connect((self.blocks_repack_bits_bb_1, 0), (self.blocks_tag_debug_0, 0))
        self.connect((self.blocks_stream_to_tagged_stream_0, 0), (self.blocks_repack_bits_bb_0, 0))
        self.connect((self.blocks_tagged_stream_mux_0, 0), (self.blocks_pack_k_bits_bb_0, 0))
        self.connect((self.blocks_tagged_stream_mux_0, 0), (self.blocks_tag_debug_0_0, 0))
        self.connect((self.digital_gmsk_demod_0, 0), (self.blocks_tag_debug_0_0_0, 0))
        self.connect((self.digital_gmsk_demod_0, 0), (self.digital_header_payload_demux_0, 0))
        self.connect((self.digital_gmsk_mod_0, 0), (self.digital_gmsk_demod_0, 0))
        self.connect((self.digital_header_payload_demux_0, 1), (self.blocks_repack_bits_bb_1, 0))
        self.connect((self.digital_header_payload_demux_0, 0), (self.digital_packet_headerparser_b_default_0, 0))
        self.connect((self.digital_packet_headergenerator_bb_default_0, 0), (self.blocks_tagged_stream_mux_0, 0))

    def get_hdr_format(self):
        return self.hdr_format

    def set_hdr_format(self, hdr_format):
        self.hdr_format = hdr_format

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps

    def get_sdr_gain(self):
        return self.sdr_gain

    def set_sdr_gain(self, sdr_gain):
        self.sdr_gain = sdr_gain

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_packet_len(self):
        return self.packet_len

    def set_packet_len(self, packet_len):
        self.packet_len = packet_len
        self.blocks_stream_to_tagged_stream_0.set_packet_len(self.packet_len)
        self.blocks_stream_to_tagged_stream_0.set_packet_len_pmt(self.packet_len)

    def get_length_tag_key(self):
        return self.length_tag_key

    def set_length_tag_key(self, length_tag_key):
        self.length_tag_key = length_tag_key

    def get_hsize(self):
        return self.hsize

    def set_hsize(self, hsize):
        self.hsize = hsize

    def get_centre_freq(self):
        return self.centre_freq

    def set_centre_freq(self, centre_freq):
        self.centre_freq = centre_freq

    def get_cal_bw(self):
        return self.cal_bw

    def set_cal_bw(self, cal_bw):
        self.cal_bw = cal_bw


def argument_parser():
    description = 'Flowgraph to test an MSK loopback without the Packet Encoder/Decoder on the LimeSDR'
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option, description=description)
    return parser


def main(top_block_cls=experimental_msk_loopback_hw, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()

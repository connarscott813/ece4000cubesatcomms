# Loopback Test
This folder contains the ini files needed to run the LimeSDR Quick Test described on:
https://wiki.myriadrf.org/LimeSDR-USB_Quick_Test

NOTE: This test is only compatible with the LimeSDR-USB. It will not work with the Mini
